use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Unit {
    letter: char,
    polarity: bool,
}

impl Unit {
    fn new(input: char) -> Unit {
        let letter = input.to_ascii_lowercase();
        Unit {
            letter,
            polarity: letter == input,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Polymer(Vec<Unit>);

impl Polymer {
    fn from_str(s: &str) -> Polymer {
        Polymer(
            s.chars()
                .filter(|c| !c.is_ascii_whitespace())
                .map(Unit::new)
                .collect(),
        )
    }

    fn react(&self) -> Polymer {
        let mut units = Vec::new();
        for &unit in self.0.iter() {
            enum Action {
                Drop,
                Push,
            }
            let action = match units.last() {
                Some(&Unit { letter, polarity })
                    if letter == unit.letter && polarity != unit.polarity =>
                {
                    Action::Drop
                }
                _ => Action::Push,
            };
            match action {
                Action::Drop => {
                    units.pop();
                }
                Action::Push => {
                    units.push(unit);
                }
            }
        }
        Polymer(units)
    }

    fn remove_unit(&self, letter: char) -> Polymer {
        Polymer(
            self.0
                .iter()
                .cloned()
                .filter(|unit| !unit.letter.eq_ignore_ascii_case(&letter))
                .collect(),
        )
    }
}

impl std::fmt::Display for Polymer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = self
            .0
            .iter()
            .map(|unit| {
                if unit.polarity {
                    unit.letter
                } else {
                    unit.letter.to_ascii_uppercase()
                }
            })
            .collect::<String>();
        write!(f, "{}", s)
    }
}

#[aoc_generator(day5)]
fn input_generator(input: &str) -> Polymer {
    Polymer::from_str(input)
}

#[aoc(day5, part1)]
fn part1(input: &Polymer) -> usize {
    input.react().0.len()
}

#[aoc(day5, part2)]
fn part2(input: &Polymer) -> usize {
    let letters = input
        .0
        .iter()
        .map(|unit| unit.letter)
        .collect::<HashSet<_>>();
    letters
        .into_iter()
        .map(|l| input.remove_unit(l))
        .map(|p| p.react().0.len())
        .min()
        .unwrap()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_from_str() {
        assert_eq!(format!("{}", Polymer::from_str("aabAAB")), "aabAAB");
    }

    #[test]
    fn test_react() {
        assert_eq!(
            Polymer::from_str("dabAcCaCBAcCcaDA").react(),
            Polymer::from_str("dabCBAcaDA")
        );
    }

    #[test]
    fn test_remove_unit() {
        assert_eq!(format!("{}", Polymer::from_str("aA").remove_unit('a')), "");
        assert_eq!(
            format!("{}", Polymer::from_str("aA").remove_unit('b')),
            "aA"
        );
        assert_eq!(
            format!("{}", Polymer::from_str("dabAcCaCBAcCcaDA").remove_unit('a')),
            "dbcCCBcCcD"
        );
        assert_eq!(
            format!("{}", Polymer::from_str("dabAcCaCBAcCcaDA").remove_unit('b')),
            "daAcCaCAcCcaDA"
        );
    }
}
