use crate::helpers::*;
use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Vector {
    x: isize,
    y: isize,
}

impl std::ops::Add for Vector {
    type Output = Vector;
    fn add(self, rhs: Vector) -> Vector {
        Vector {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::AddAssign for Vector {
    fn add_assign(&mut self, rhs: Vector) {
        *self = *self + rhs;
    }
}

impl std::ops::Mul<isize> for Vector {
    type Output = Vector;
    fn mul(self, rhs: isize) -> Vector {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Rect {
    origin: Vector,
    size: Vector,
}

impl Rect {
    fn max_x(&self) -> isize {
        self.origin.x + self.size.x
    }

    fn max_y(&self) -> isize {
        self.origin.y + self.size.y
    }

    fn area(&self) -> isize {
        self.size.x * self.size.y
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Record {
    position: Vector,
    velocity: Vector,
}

impl Record {
    fn from_str(s: &str) -> Record {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r#"(?x)
                ^
                position=<\s*(-?\d+),\s*(-?\d+)>
                \s+
                velocity=<\s*(-?\d+),\s*(-?\d+)>
                $"#
            )
            .unwrap();
        }
        let caps = RE.captures(s).expect("regex didn't match input");
        let position = Vector {
            x: caps.parse_unwrap(1),
            y: caps.parse_unwrap(2),
        };
        let velocity = Vector {
            x: caps.parse_unwrap(3),
            y: caps.parse_unwrap(4),
        };
        Record { position, velocity }
    }
}

#[derive(Debug, Clone)]
struct Sky(Vec<Record>);

impl Sky {
    fn show(&self) -> String {
        let bounds = self.bounds();
        assert!(bounds.size.x < 200, "sky is too wide");
        assert!(bounds.size.y < 200, "sky is too tall");
        let lights = self.0.iter().map(|r| r.position).collect::<HashSet<_>>();
        (bounds.origin.y..=bounds.max_y())
            .map(|y| {
                (bounds.origin.x..=bounds.max_x())
                    .map(|x| {
                        if lights.contains(&Vector { x, y }) {
                            '#'
                        } else {
                            '.'
                        }
                    })
                    .chain(Some('\n'))
                    .collect::<String>()
            })
            .collect()
    }

    fn step(&mut self, count: isize) {
        for record in self.0.iter_mut() {
            record.position += record.velocity * count;
        }
    }

    fn bounds(&self) -> Rect {
        if self.0.is_empty() {
            panic!("Can't get bounds of an empty sky")
        }
        let first_pos = self.0[0].position;
        let (min_x, max_x, min_y, max_y) = self.0.iter().fold(
            (first_pos.x, first_pos.x, first_pos.y, first_pos.y),
            |accum, record| {
                (
                    accum.0.min(record.position.x),
                    accum.1.max(record.position.x),
                    accum.2.min(record.position.y),
                    accum.3.max(record.position.y),
                )
            },
        );
        Rect {
            origin: Vector { x: min_x, y: min_y },
            size: Vector {
                x: max_x - min_x,
                y: max_y - min_y,
            },
        }
    }

    /// Steps the sky until it reaches its smallest bounds.
    /// Returns the number of steps.
    fn step_to_smallest(&mut self) -> usize {
        let mut area = self.bounds().area();
        let mut count = 0;
        loop {
            self.step(1);
            let new_area = self.bounds().area();
            if new_area > area {
                self.step(-1);
                break;
            } else {
                area = new_area;
                count += 1;
            }
        }
        count
    }
}

#[aoc_generator(day10)]
fn input_generator(input: &str) -> Sky {
    Sky(input.lines().map(Record::from_str).collect())
}

#[aoc(day10, part1)]
fn part1(input: &Sky) -> String {
    let mut sky = input.clone();
    // step the sky until we get the smallest sky. We're assuming this will be the message.
    sky.step_to_smallest();
    format!(
        "\n{}", // ensure cargo-aoc prints it starting at column 0
        sky.show()
    )
}

#[aoc(day10, part2)]
fn part2(input: &Sky) -> usize {
    let mut sky = input.clone();
    sky.step_to_smallest()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_record_from_str() {
        assert_eq!(
            Record::from_str("position=< -9951, -50547> velocity=< 1,  5>"),
            Record {
                position: Vector {
                    x: -9951,
                    y: -50547
                },
                velocity: Vector { x: 1, y: 5 }
            }
        );
        assert_eq!(
            Record::from_str("position=< 40751,  30545> velocity=<-4, -3>"),
            Record {
                position: Vector { x: 40751, y: 30545 },
                velocity: Vector { x: -4, y: -3 }
            }
        );
    }

    #[test]
    fn test_show_sky() {
        let make_rec = |x, y| Record {
            position: Vector { x, y },
            velocity: Vector { x: 0, y: 0 },
        };
        let records = vec![make_rec(5, 5), make_rec(6, 6), make_rec(7, 7)];
        assert_eq!(Sky(records).show(), "#..\n.#.\n..#\n");
    }
}
