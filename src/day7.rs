use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeMap, HashSet};

#[derive(Debug, Clone)]
struct Graph {
    nodes: BTreeMap<char, Vec<char>>,
    completed: HashSet<char>,
}

impl Graph {
    // Item is &char to retain the lifetime
    fn iter_available(&self) -> impl Iterator<Item=&char> {
        self.nodes
            .iter()
            .filter(move |(letter, _)| !self.completed.contains(&letter))
            .filter(move |(_, dependencies)| dependencies.iter().all(|c| self.completed.contains(&c)))
            .map(|(letter, _)| letter)
    }

    fn mark_completed(&mut self, letter: char) {
        self.completed.insert(letter);
    }

    fn complete_next(&mut self) -> Option<char> {
        let next = self.iter_available().cloned().next();
        if let Some(letter) = next {
            self.mark_completed(letter);
        }
        next
    }

    fn into_iter_completed(self) -> impl Iterator<Item = char> {
        struct Iter(Graph);
        impl Iterator for Iter {
            type Item = char;
            fn next(&mut self) -> Option<char> {
                self.0.complete_next()
            }
        }
        Iter(self)
    }
}

#[aoc_generator(day7)]
fn input_generator(input: &str) -> Graph {
    let mut nodes = BTreeMap::<char, Vec<char>>::new();
    for line in input.lines() {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"^Step (\w) must be finished before step (\w) can begin.$").unwrap();
        }
        let caps = RE.captures(line).expect("Couldn't match input line");
        let dependency = caps.get(1).unwrap().as_str().chars().next().unwrap();
        let dependent = caps.get(2).unwrap().as_str().chars().next().unwrap();
        nodes.entry(dependent).or_default().push(dependency);
        nodes.entry(dependency).or_default(); // make sure the initial nodes are available
    }
    Graph {
        nodes,
        completed: HashSet::new(),
    }
}

#[aoc(day7, part1)]
fn part1(input: &Graph) -> String {
    let graph = input.clone();
    graph.into_iter_completed().collect()
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Task {
    letter: char,
    duration: u32,
}

#[derive(Debug)]
struct Scheduler {
    tasks: Vec<Task>,
    num_workers: usize,
    graph: Graph,
    total_duration: u32,
    completions: String,
}

impl Scheduler {
    fn new(graph: Graph, num_workers: usize) -> Scheduler {
        let mut scheduler = Scheduler {
            tasks: Vec::new(),
            num_workers,
            graph,
            total_duration: 0,
            completions: String::new(),
        };
        scheduler.refill_tasks();
        scheduler
    }

    fn refill_tasks(&mut self) {
        if self.tasks.len() >= self.num_workers { return; }
        for &letter in self.graph.iter_available() {
            if self.tasks.iter().find(|t| t.letter == letter).is_some() { continue; }
            let duration = (letter as u32) - ('A' as u32) + 61;
            self.tasks.push(Task { letter, duration });
            if self.tasks.len() >= self.num_workers { break; }
        }
    }

    fn step(&mut self) -> bool {
        if { self.tasks.len() == 0 } { return false; }
        self.total_duration += 1;
        for task in self.tasks.iter_mut() {
            task.duration -= 1;
            if task.duration == 0 {
                self.completions.push(task.letter);
                self.graph.mark_completed(task.letter);
            }
        }
        self.tasks.retain(|t| t.duration > 0);
        self.refill_tasks();
        !self.tasks.is_empty()
    }

    fn run_to_completion(&mut self) -> u32 {
        while self.step() {}
        self.total_duration
    }
}

#[aoc(day7, part2)]
fn part2(input: &Graph) -> u32 {
    Scheduler::new(input.clone(), 5).run_to_completion()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn make_sample_graph() -> Graph {
        Graph {
            nodes: vec![
                ('A', vec!['C']),
                ('B', vec!['A']),
                ('C', vec![]),
                ('D', vec!['A']),
                ('E', vec!['B', 'D', 'F']),
                ('F', vec!['C']),
            ]
            .into_iter()
            .collect(),
            completed: HashSet::new(),
        }
    }

    #[test]
    fn test_complete_next() {
        let mut graph = make_sample_graph();
        assert_eq!(graph.complete_next(), Some('C'));
        assert_eq!(graph.complete_next(), Some('A'));
        assert_eq!(graph.complete_next(), Some('B'));
        assert_eq!(graph.complete_next(), Some('D'));
        assert_eq!(graph.complete_next(), Some('F'));
        assert_eq!(graph.complete_next(), Some('E'));
        assert_eq!(graph.complete_next(), None);
    }

    #[test]
    fn test_into_iter_completed() {
        let graph = make_sample_graph();
        assert_eq!(graph.into_iter_completed().collect::<String>(), "CABDFE");
    }
}
