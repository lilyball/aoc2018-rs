use aoc_runner_derive::{aoc, aoc_generator};
use regex::Regex;
use std::cell::RefCell;
use std::rc::Rc;

// This is a very weird one. We need to simulate a circle of marbles. To do this we're going to use
// a doubly-linked circular list, so we can go around the list as many times as necessary.

#[derive(Clone)]
struct _Node {
    value: usize,
    next: RefCell<Node>,
    prev: RefCell<Node>,
}

impl std::fmt::Debug for _Node {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.value.fmt(f)
    }
}

#[derive(Debug, Clone)]
struct Node(Rc<_Node>);

impl std::ops::Deref for Node {
    type Target = Rc<_Node>;
    fn deref(&self) -> &Rc<_Node> {
        &self.0
    }
}

impl Node {
    fn new(value: usize) -> Node {
        // This is really gross
        unsafe {
            // assume Rc::new cannot panic
            let node = Node(Rc::new(_Node {
                value,
                next: RefCell::new(std::mem::uninitialized()),
                prev: RefCell::new(std::mem::uninitialized()),
            }));
            // Rely on Rc.clone not touching the value
            std::ptr::write(node.next.as_ptr(), node.clone());
            std::ptr::write(node.prev.as_ptr(), node.clone());
            node
        }
    }

    /// Insert the given node after the current one.
    ///
    /// This throws away the old prev/next pointers on the new node.
    fn insert_after(&self, node: Node) {
        let old_next = self.next.replace(node);
        *old_next.prev.borrow_mut() = self.next.borrow().clone();
        *self.next.borrow().next.borrow_mut() = old_next;
        *self.next.borrow().prev.borrow_mut() = self.clone();
    }

    /// Removes the node from the list.
    fn remove(&self) {
        if !Rc::ptr_eq(&self.next.borrow(), self) {
            let old_next = self.next.replace(self.clone());
            let old_prev = self.prev.replace(self.clone());
            *old_next.prev.borrow_mut() = old_prev.clone(); // can't seem to skip the clone
            *old_prev.next.borrow_mut() = old_next;
        }
    }

    fn next(&self, mut offset: usize) -> Node {
        // for the time being, just live with cloning each node as we walk. If we want to work on
        // performance we can try to avoid cloning the intermediate nodes later.
        if offset == 0 {
            return self.clone();
        }
        let mut node = self.next.borrow().clone();
        offset -= 1;
        while offset > 0 {
            let next = node.next.borrow().clone();
            node = next;
            offset -= 1;
        }
        node
    }

    fn prev(&self, mut offset: usize) -> Node {
        if offset == 0 {
            return self.clone();
        }
        let mut node = self.prev.borrow().clone();
        offset -= 1;
        while offset > 0 {
            let next = node.prev.borrow().clone();
            node = next;
            offset -= 1;
        }
        node
    }
}

#[derive(Debug, Copy, Clone)]
struct Input {
    players: usize,
    last_marble: usize,
}

#[aoc_generator(day9)]
fn input_generator(input: &str) -> Input {
    let regex = Regex::new(r#"^(\d+) players; last marble is worth (\d+) points$"#).unwrap();
    let caps = regex.captures(input).unwrap();
    let players = caps.get(1).unwrap().as_str().parse().unwrap();
    let last_marble = caps.get(2).unwrap().as_str().parse().unwrap();
    Input {
        players,
        last_marble,
    }
}

fn play_game(input: Input) -> usize {
    let mut scores = vec![0; input.players];
    let mut current = Node::new(0);
    for (marble, elf) in (1..=input.last_marble).zip((0..scores.len()).cycle()) {
        if marble % 23 == 0 {
            scores[elf] += marble;
            let back6 = current.prev(6);
            let back7 = back6.prev(1);
            back7.remove();
            scores[elf] += back7.value;
            current = back6;
        } else {
            let new_node = Node::new(marble);
            current.next(1).insert_after(new_node.clone());
            current = new_node;
        }
    }
    scores.into_iter().max().unwrap()
}

#[aoc(day9, part1)]
fn part1(input: &Input) -> usize {
    play_game(*input)
}

#[aoc(day9, part2)]
fn part2(input: &Input) -> usize {
    let mut input = *input;
    input.last_marble *= 100;
    play_game(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_insert_after() {
        let node = Node::new(0); // solo node
        assert!(Rc::ptr_eq(&node.next.borrow(), &node));
        assert!(Rc::ptr_eq(&node.prev.borrow(), &node));

        node.insert_after(Node::new(1)); // two nodes
        let node1 = node.next.borrow().clone();
        assert!(
            Rc::ptr_eq(&node.prev.borrow(), &node1),
            "node.prev is {:?}, expected {:?}",
            &node.prev.borrow(),
            node1
        );
        assert!(Rc::ptr_eq(&node1.next.borrow(), &node));
        assert!(Rc::ptr_eq(&node1.prev.borrow(), &node));

        node.insert_after(Node::new(2)); // three nodes
        let node2 = node.next.borrow().clone();
        assert!(Rc::ptr_eq(&node.prev.borrow(), &node1));
        assert!(Rc::ptr_eq(&node2.next.borrow(), &node1));
        assert!(Rc::ptr_eq(&node2.prev.borrow(), &node));
    }

    #[test]
    fn test_remove() {
        let node = Node::new(0); // solo node
        node.remove();
        assert!(Rc::ptr_eq(&node.next.borrow(), &node));
        assert!(Rc::ptr_eq(&node.prev.borrow(), &node));

        node.insert_after(Node::new(2));
        node.insert_after(Node::new(1));
        node.next(1).remove();
        assert_eq!(node.next(1).value, 2);
        assert_eq!(node.next(2).value, 0);
        assert_eq!(node.prev(1).value, 2);
        assert_eq!(node.prev(2).value, 0);
    }

    #[test]
    fn test_node_next() {
        let node = Node::new(0); // solo node
        assert_eq!(node.next(0).value, 0);
        assert_eq!(node.next(5).value, 0);

        node.insert_after(Node::new(2));
        node.insert_after(Node::new(1)); // now three nodes
        assert_eq!(node.next(0).value, 0);
        assert_eq!(node.next(1).value, 1);
        assert_eq!(node.next(2).value, 2);
        assert_eq!(node.next(3).value, 0);
        assert_eq!(node.next(4).value, 1);
        assert_eq!(node.next(5).value, 2);
        assert_eq!(node.next(6).value, 0);
    }

    #[test]
    fn test_node_prev() {
        let node = Node::new(0); // solo node
        assert_eq!(node.prev(0).value, 0);
        assert_eq!(node.prev(5).value, 0);

        node.insert_after(Node::new(2));
        node.insert_after(Node::new(1)); // now three nodes
        assert_eq!(node.prev(0).value, 0);
        assert_eq!(node.prev(1).value, 2);
        assert_eq!(node.prev(2).value, 1);
        assert_eq!(node.prev(3).value, 0);
        assert_eq!(node.prev(4).value, 2);
        assert_eq!(node.prev(5).value, 1);
    }
}
