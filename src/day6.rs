use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, HashSet};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Coord {
    x: usize,
    y: usize,
}

impl Coord {
    fn parse(s: &str) -> Coord {
        let comma_idx = s.find(',').unwrap();
        let x = s[0..comma_idx].parse().unwrap();
        let y = s[(comma_idx + 2)..].parse().unwrap();
        Coord { x, y }
    }

    fn distance_to(&self, other: &Coord) -> usize {
        let x_dist = if self.x > other.x {
            self.x - other.x
        } else {
            other.x - self.x
        };
        let y_dist = if self.y > other.y {
            self.y - other.y
        } else {
            other.y - self.y
        };
        x_dist + y_dist
    }
}

#[aoc_generator(day6)]
fn input_generator(input: &str) -> Vec<Coord> {
    input.lines().map(Coord::parse).collect()
}

// Given our input, a 358x355 grid is large enough to cover all coordinates
const WIDTH: usize = 358;
const HEIGHT: usize = 355;

#[aoc(day6, part1)]
fn part1(input: &[Coord]) -> usize {
    let mut grid = [[None; WIDTH]; HEIGHT];
    // Mark up the grid where each cell is the index of the closest coordinate.
    // Also determine infinite areas by looking at the grid edges.
    // Any area that touches an edge will extend infinitely.
    let mut infinite = HashSet::new();
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            let cell_coord = Coord { x, y };
            let mut min_value = (0, usize::max_value(), false);
            for (idx, dist) in input.iter().map(|c| cell_coord.distance_to(c)).enumerate() {
                if dist == min_value.1 {
                    min_value.2 = true;
                } else if dist < min_value.1 {
                    min_value = (idx, dist, false);
                }
            }
            if !min_value.2 {
                grid[y][x] = Some(min_value.0);
                if x == 0 || x + 1 == WIDTH || y == 0 || y + 1 == HEIGHT {
                    infinite.insert(min_value.0);
                }
            }
        }
    }
    // Wipe all infinite areas from the grid.
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            if let Some(idx) = grid[y][x] {
                if infinite.contains(&idx) {
                    grid[y][x] = None;
                }
            }
        }
    }
    // Size up the areas
    let mut sizes = HashMap::new();
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            if let Some(idx) = grid[y][x] {
                *sizes.entry(idx).or_default() += 1;
            }
        }
    }
    sizes.values().cloned().max().unwrap()
}

#[aoc(day6, part2)]
fn part2(input: &[Coord]) -> usize {
    let mut grid = [[0; WIDTH]; HEIGHT];
    // Mark up the grid so each cell is the total distance to all coordinates
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            let cell_coord = Coord { x, y };
            grid[y][x] = input.iter().map(|c| cell_coord.distance_to(c)).sum();
        }
    }
    const MAX_DIST: usize = 10000;
    // Count all all cells whose max distance is less than the limit
    (0..HEIGHT)
        .map(|y| {
            (0..WIDTH)
                .map(|x| grid[y][x])
                .filter(|&dist| dist < MAX_DIST)
                .count()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_distance_to() {
        assert_eq!(Coord { x: 1, y: 1 }.distance_to(&Coord { x: 1, y: 1 }), 0);
        assert_eq!(Coord { x: 1, y: 1 }.distance_to(&Coord { x: 2, y: 1 }), 1);
        assert_eq!(Coord { x: 1, y: 1 }.distance_to(&Coord { x: 5, y: 5 }), 8);
        assert_eq!(Coord { x: 5, y: 5 }.distance_to(&Coord { x: 1, y: 1 }), 8);
        assert_eq!(Coord { x: 4, y: 4 }.distance_to(&Coord { x: 3, y: 4 }), 1);
        assert_eq!(Coord { x: 4, y: 4 }.distance_to(&Coord { x: 5, y: 5 }), 2);
    }
}
