use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;

#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<i32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
fn part1(input: &[i32]) -> i32 {
    input.iter().sum()
}

#[aoc(day1, part2)]
fn part2(input: &[i32]) -> i32 {
    let mut seen = HashSet::new();
    seen.insert(0);
    input
        .iter()
        .cycle()
        .scan(0, |state, &x| {
            *state += x;
            Some(*state)
        })
        .find(move |&freq| !seen.insert(freq))
        .unwrap()
}
