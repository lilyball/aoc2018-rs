#![allow(unused)]
use crate::grid::*;
use crate::helpers::*;
use crate::scanner::*;
use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use rayon::prelude::*;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::iter::repeat;
use std::mem::replace;

#[aoc_generator(day0)]
fn input_generator(input: &str) -> () {}

#[aoc(day0, part1)]
fn part1(input: &()) -> () {}

#[cfg(test)]
mod tests {
    use super::*;
}
