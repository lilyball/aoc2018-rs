use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Clone, PartialEq, Eq, Hash)]
struct Pots {
    pos: Vec<bool>,
    neg: Vec<bool>
}

#[derive(Clone)]
struct Rack {
    pots: Pots,
    rules: Vec<Rule>,
}

#[derive(Copy, Clone)]
struct Rule {
    input: [bool; 5],
    output: bool,
}

impl Pots {
    fn from_str(input: &str) -> Pots {
        let pots = input
            .chars()
            .map(|c| match c {
                '#' => true,
                '.' => false,
                _ => panic!("unknown state char: {}", c),
            })
            .collect();
        Pots {
            pos: pots,
            neg: Vec::new(),
        }
    }

    fn get(&self, index: isize) -> bool {
        if index >= 0 {
            self.pos.get(index as usize).map_or(false, |&x| x)
        } else {
            self.neg.get(-index as usize - 1).map_or(false, |&x| x)
        }
    }

    fn set(&mut self, index: isize, value: bool) {
        let helper = move |index: usize, pots: &mut Vec<bool>| {
            if pots.len() <= index {
                if !value {
                    return;
                }
                pots.extend(std::iter::repeat(false).take(index - pots.len() + 1));
            }
            pots[index] = value
        };
        if index >= 0 {
            helper(index as usize, &mut self.pos);
        } else {
            helper(-index as usize - 1, &mut self.neg);
        }
    }

    /// Returns the smallest range that covers all plants.
    fn bounds(&self) -> std::ops::Range<isize> {
        let lower_bound = match self.neg.iter().enumerate().find(|&(_, &x)| x) {
            Some((i, _)) => -(i as isize + 1),
            None => match self.pos.iter().enumerate().find(|&(_, &x)| x) {
                Some((i, _)) => i as isize,
                None => {
                    return 0..0;
                }
            },
        };
        let upper_bound = match self.pos.iter().enumerate().rev().find(|&(_, &x)| x) {
            Some((i, _)) => i as isize,
            None => match self.neg.iter().enumerate().rev().find(|&(_, &x)| x) {
                Some((i, _)) => -(i as isize + 1),
                None => panic!("we have a lower bound but not an upper bound?!"),
            },
        };
        lower_bound..(upper_bound + 1)
    }
}

impl std::fmt::Display for Pots {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let helper = |bounds: std::ops::Range<isize>| {
            bounds
                .map(|i| if self.get(i) { '#' } else { '.' })
                .collect::<String>()
        };
        let bounds = self.bounds();
        let neg = if bounds.start < 0 {
            helper(bounds.start..0)
        } else {
            "".to_owned()
        };
        let pos = if bounds.end >= 0 {
            helper(0..bounds.end)
        } else {
            "".to_owned()
        };
        if !neg.is_empty() {
            write!(f, "{}", neg)?;
            if !pos.is_empty() {
                write!(f, " ")?;
            }
        }
        write!(f, "{}", pos)
    }
}

impl Rack {
    fn from_str(input: &str) -> Rack {
        let mut lines = input.lines();
        let state_str = lines.next().unwrap().trim_start_matches("initial state: ");
        let rules = lines
            .filter(|s| !s.is_empty())
            .map(Rule::from_str)
            .collect::<Vec<_>>();
        assert!(!rules
            .iter()
            .any(|r| r.input == [false, false, false, false, false] && r.output));
        Rack {
            pots: Pots::from_str(state_str),
            rules,
        }
    }

    fn get(&self, index: isize) -> bool {
        self.pots.get(index)
    }

    fn set(&mut self, index: isize, value: bool) {
        self.pots.set(index, value)
    }

    /// Returns the smallest range that covers all plants.
    fn bounds(&self) -> std::ops::Range<isize> {
        self.pots.bounds()
    }

    fn apply_rules(&mut self) {
        let old_pots = self.pots.clone();
        let bounds = self.bounds();
        for i in (bounds.start-3)..(bounds.end+3) {
            let input = [
                old_pots.get(i - 2),
                old_pots.get(i - 1),
                old_pots.get(i),
                old_pots.get(i + 1),
                old_pots.get(i + 2),
            ];
            if let Some(output) = self.rules.iter().find_map(|rule| rule.apply(input)) {
                self.set(i, output)
            } else {
                // We should have rules for all patterns, but the sample input doesn't
                self.set(i, false)
            }
        }
    }

    fn sum_planted_pots(&self) -> isize {
        self.bounds().filter(|&i| self.get(i)).sum()
    }
}

impl Rule {
    fn from_str(input: &str) -> Rule {
        lazy_static! {
            static ref RE: Regex = Regex::new(r#"^([#.]+) => ([#.])$"#).unwrap();
        }
        let caps = RE.captures(input).expect("rule regex did not match");
        let input = caps.get(1).unwrap().as_str();
        let output = caps.get(2).unwrap().as_str();
        assert!(
            input.len() == 5,
            "rule input had unexpected length {}",
            input.len()
        );
        assert!(
            output.len() == 1,
            "rule output had unexpected length {}",
            output.len()
        );
        let mut inputs = input.chars().map(|c| match c {
            '#' => true,
            '.' => false,
            _ => panic!("unexpected rule input char: {}", c),
        });
        let output = match output.chars().next().unwrap() {
            '#' => true,
            '.' => false,
            c => panic!("unexpected rule output char: {}", c),
        };
        Rule {
            input: [
                inputs.next().unwrap(),
                inputs.next().unwrap(),
                inputs.next().unwrap(),
                inputs.next().unwrap(),
                inputs.next().unwrap(),
            ],
            output,
        }
    }

    fn apply(&self, pots: [bool; 5]) -> Option<bool> {
        if pots.iter().cloned().zip(&self.input).all(|(a, &b)| a == b) {
            Some(self.output)
        } else {
            None
        }
    }
}

#[aoc_generator(day12)]
fn input_generator(input: &str) -> Rack {
    Rack::from_str(input)
}

#[aoc(day12, part1)]
fn part1(input: &Rack) -> isize {
    let mut rack = input.clone();
    for _ in 0..20 {
        rack.apply_rules();
    }
    rack.sum_planted_pots()
}

#[aoc(day12, part2)]
fn part2(input: &Rack) -> i64 {
    let mut rack = input.clone();
    // it appears the sum converges on some multiple of the generation plus a constant.
    // Calculate what that is. For my input it converged within 1000 generations
    // (I didn't check any sooner), but to be safe I'll just check every 1000 generations and stop
    // when I've seen the same multiple twice in a row.
    for _ in 1..=1000 {
        rack.apply_rules();
    }
    let (mut multiple, mut constant) = {
        let sum = rack.sum_planted_pots();
        (sum / 1000, sum % 1000)
    };
    for i in 1001..=50000000000i64 {
        rack.apply_rules();
        if i % 1000 != 0 { continue; }
        let (multiple_, constant_) = {
            let sum = rack.sum_planted_pots();
            (sum / i as isize, sum % i as isize)
        };
        if (multiple, constant) == (multiple_, constant_) {
            break;
        } else {
            multiple = multiple_;
            constant = constant_;
        }
    }
    50000000000i64 * (multiple as i64) + (constant as i64)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let sample_input = r#"initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"#;
        let mut rack = Rack::from_str(sample_input);
        assert_eq!(format!("{}", rack.pots), "#..#.#..##......###...###"); // 0
        rack.apply_rules();
        assert_eq!(format!("{}", rack.pots), "#...#....#.....#..#..#..#"); // 1
        rack.apply_rules();
        assert_eq!(format!("{}", rack.pots), "##..##...##....#..#..#..##"); // 2
        rack.apply_rules();
        assert_eq!(format!("{}", rack.pots), "# .#...#..#.#....#..#..#...#"); // 3
        rack.apply_rules();
        assert_eq!(format!("{}", rack.pots), "#.#..#...#.#...#..#..##..##"); // 4

        for _ in 5..=20 {
            rack.apply_rules();
        }
        assert_eq!(format!("{}", rack.pots), "#. ...##....#####...#######....#.#..##"); // 20
        assert_eq!(rack.sum_planted_pots(), 325);
    }
}
