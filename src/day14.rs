use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day14)]
fn input_generator(input: &str) -> usize {
    input.parse().unwrap()
}

#[aoc(day14, part1)]
fn part1(input: &usize) -> String {
    let mut recipes = vec![3u8, 7];
    let mut elf1 = 0usize;
    let mut elf2 = 1usize;
    while recipes.len() < *input + 10 {
        let sum = recipes[elf1] + recipes[elf2];
        if sum >= 10 {
            recipes.push(sum / 10);
        }
        recipes.push(sum % 10);
        elf1 = (elf1 + 1 + recipes[elf1] as usize) % recipes.len();
        elf2 = (elf2 + 1 + recipes[elf2] as usize) % recipes.len();
    }
    recipes[*input..].iter().take(10).map(|r| format!("{}", r)).collect::<String>()
}

#[aoc(day14, part2)]
fn part2(input: &usize) -> usize {
    let needle = format!("{}", *input).chars().map(|c| (c as u8) - b'0').collect::<Vec<_>>();

    let mut recipes = vec![3u8, 7];
    let mut elf1 = 0usize;
    let mut elf2 = 1usize;
    loop {
        for _ in 0..1000000 {
            let sum = recipes[elf1] + recipes[elf2];
            if sum >= 10 {
                recipes.push(sum / 10);
            }
            recipes.push(sum % 10);
            elf1 = (elf1 + 1 + recipes[elf1] as usize) % recipes.len();
            elf2 = (elf2 + 1 + recipes[elf2] as usize) % recipes.len();
        }
        for (i, window) in recipes.windows(needle.len()).enumerate() {
            if window == &*needle {
                return i;
            }
        }
    }
}
