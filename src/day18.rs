use crate::grid::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Tile {
    Ground,
    Trees,
    Lumberyard,
}

impl Tile {
    fn from_char(c: char) -> Tile {
        match c {
            '.' => Tile::Ground,
            '|' => Tile::Trees,
            '#' => Tile::Lumberyard,
            _ => panic!("unknown tile type {}", c),
        }
    }

    #[allow(unused)]
    fn as_char(self) -> char {
        match self {
            Tile::Ground => '.',
            Tile::Trees => '|',
            Tile::Lumberyard => '#',
        }
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
struct Area(Grid<Tile>);

impl Area {
    fn from_str(input: &str) -> Area {
        let width = input.lines().map(|line| line.len()).max().unwrap();
        let height = input.lines().count();
        let mut tiles = Grid::new_clone(Size::new(width as i32, height as i32), &Tile::Ground);
        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                tiles[Coord::new(x as i32, y as i32)] = Tile::from_char(c);
            }
        }
        Area(tiles)
    }

    fn count_tiles_of_type(&self, tile: Tile) -> usize {
        self.0.iter().filter(|(_, &tile_)| tile == tile_).count()
    }

    fn tick(&mut self) -> Area {
        let mut output = self.0.clone();
        const OFFSETS: [Coord; 8] = [
            Coord::new(-1, -1),
            Coord::new(0, -1),
            Coord::new(1, -1),
            Coord::new(-1, 0),
            Coord::new(1, 0),
            Coord::new(-1, 1),
            Coord::new(0, 1),
            Coord::new(1, 1),
        ];
        for (coord, tile) in output.iter_mut() {
            match *tile {
                Tile::Ground => {
                    let mut adj_trees = OFFSETS
                        .iter()
                        .filter_map(|c| self.0.get(coord + c))
                        .filter(|&&tile| tile == Tile::Trees);
                    if adj_trees.nth(2).is_some() {
                        *tile = Tile::Trees;
                    }
                }
                Tile::Trees => {
                    let mut adj_lumber = OFFSETS
                        .iter()
                        .filter_map(|c| self.0.get(coord + c))
                        .filter(|&&tile| tile == Tile::Lumberyard);
                    if adj_lumber.nth(2).is_some() {
                        *tile = Tile::Lumberyard;
                    }
                }
                Tile::Lumberyard => {
                    let mut adj_trees = OFFSETS
                        .iter()
                        .filter_map(|c| self.0.get(coord + c))
                        .filter(|&&tile| tile == Tile::Trees);
                    let mut adj_lumber = OFFSETS
                        .iter()
                        .filter_map(|c| self.0.get(coord + c))
                        .filter(|&&tile| tile == Tile::Lumberyard);
                    if adj_trees.next().is_none() || adj_lumber.next().is_none() {
                        *tile = Tile::Ground;
                    }
                }
            }
        }
        Area(output)
    }
}

#[aoc_generator(day18)]
fn input_generator(input: &str) -> Area {
    Area::from_str(input)
}

#[aoc(day18, part1)]
fn part1(input: &Area) -> usize {
    let mut area = input.clone();
    for _ in 0..10 {
        area = area.tick();
    }
    area.count_tiles_of_type(Tile::Lumberyard) * area.count_tiles_of_type(Tile::Trees)
}

#[aoc(day18, part2)]
fn part2(input: &Area) -> usize {
    let mut area = input.clone();
    let mut seen = HashMap::<Area, usize>::new();
    let target_mins = 1000000000;
    for minute in 0..target_mins {
        if let Some(past) = seen.get(&area) {
            // We've hit a loop
            let loop_len = minute - past;
            let offset = (target_mins - past) % loop_len;
            let final_area = seen
                .iter()
                .find(|&(_, &min)| min == past + offset)
                .unwrap()
                .0;
            return final_area.count_tiles_of_type(Tile::Lumberyard)
                * final_area.count_tiles_of_type(Tile::Trees);
        }
        let new_area = area.tick();
        seen.insert(area, minute);
        area = new_area;
    }
    // We never saw a duplicate
    area.count_tiles_of_type(Tile::Lumberyard) * area.count_tiles_of_type(Tile::Trees)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn area_from_str() {
        let area = Area::from_str(include_str!("../test_data/day18.txt"));
        assert_eq!(area.0.size(), Size::new(10, 10));
        assert_eq!(area.0[Coord::new(0, 0)], Tile::Ground);
        assert_eq!(area.0[Coord::new(1, 0)], Tile::Lumberyard);
        assert_eq!(area.0[Coord::new(9, 9)], Tile::Ground);
        assert_eq!(area.0[Coord::new(8, 9)], Tile::Trees);
    }

    #[test]
    fn tick() {
        let mut area = Area::from_str(include_str!("../test_data/day18.txt"));
        area = area.tick();
        for &(xy, tile) in &[
            ((1, 0), Tile::Ground),
            ((7, 0), Tile::Lumberyard),
            ((8, 0), Tile::Lumberyard),
            ((9, 0), Tile::Ground),
            ((6, 1), Tile::Trees),
            ((0, 6), Tile::Trees),
            ((8, 9), Tile::Trees),
            ((9, 9), Tile::Ground),
        ] {
            assert_eq!(area.0[xy.into()], tile, "coord {:?}", xy);
        }
    }
}
