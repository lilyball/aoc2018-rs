use aoc_runner_derive::{aoc, aoc_generator};
use rayon::prelude::*;

#[derive(Clone)]
struct Grid {
    cells: Box<[[isize; 300]; 300]>,
}

impl Grid {
    const WIDTH: usize = 300;
    const HEIGHT: usize = 300;
    fn new(serial_number: isize) -> Grid {
        let mut cells = Box::new([[0; Grid::WIDTH]; Grid::HEIGHT]);
        for y in 0..Grid::HEIGHT {
            for x in 0..Grid::WIDTH {
                let (x_coord, y_coord) = (x as isize + 1, y as isize + 1);
                let rack_id = x_coord + 10;
                let mut power_level = rack_id * y_coord;
                power_level += serial_number;
                power_level *= rack_id;
                // keep hundreds digit
                power_level = (power_level / 100) % 10;
                power_level -= 5;
                cells[y][x] = power_level;
            }
        }
        Grid { cells }
    }

    #[cfg(test)]
    fn at(&self, x_coord: usize, y_coord: usize) -> isize {
        self.cells[y_coord - 1][x_coord - 1]
    }

    /// Returns the total power level of the N×N block identified by the given upper-left corner.
    fn block_at(&self, x_coord: usize, y_coord: usize, size: usize) -> isize {
        let (x, y) = (x_coord - 1, y_coord - 1);
        (y..Grid::HEIGHT)
            .take(size)
            .map(|y| {
                (x..Grid::WIDTH)
                    .take(size)
                    .map(|x| self.cells[y][x])
                    .sum::<isize>()
            })
            .sum()
    }
}

#[aoc_generator(day11)]
fn input_generator(input: &str) -> Grid {
    let serial_number = input.parse().unwrap();
    Grid::new(serial_number)
}

#[aoc(day11, part1)]
fn part1(input: &Grid) -> String {
    let (x, y, _) = (1..=Grid::HEIGHT)
        .flat_map(|y| (1..=Grid::WIDTH).map(move |x| (x, y, input.block_at(x, y, 3))))
        .max_by_key(|&(_, _, power)| power)
        .unwrap();
    format!("{},{}", x, y)
}

#[aoc(day11, part2)]
fn part2(input: &Grid) -> String {
    let (x, y, size, _) = (1..Grid::WIDTH.min(Grid::HEIGHT))
        .into_par_iter()
        .map(|size| {
            (1..Grid::HEIGHT + 1 - size)
                .flat_map(move |y| {
                    (1..Grid::WIDTH + 1 - size)
                        .map(move |x| (x, y, size, input.block_at(x, y, size)))
                })
                .max_by_key(|&(_, _, _, power)| power)
                .unwrap()
        })
        .max_by_key(|&(_, _, _, power)| power)
        .unwrap();
    format!("{},{},{}", x, y, size)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_grid() {
        assert_eq!(Grid::new(8).at(3, 5), 4);
        assert_eq!(Grid::new(57).at(122, 79), -5);
        assert_eq!(Grid::new(39).at(217, 196), 0);
        assert_eq!(Grid::new(71).at(101, 153), 4);
    }

    #[test]
    fn test_block_at() {
        assert_eq!(Grid::new(18).block_at(33, 45, 3), 29);
        assert_eq!(Grid::new(42).block_at(21, 61, 3), 30);
        assert_eq!(Grid::new(18).block_at(33, 45, 0), 0);
        assert_eq!(Grid::new(18).block_at(32, 44, 2), -6);
        assert_eq!(Grid::new(18).block_at(33, 45, 1), 4);
        assert_eq!(Grid::new(18).block_at(33, 45, 4), 12);
        assert_eq!(Grid::new(18).block_at(90, 269, 16), 113);
        assert_eq!(Grid::new(42).block_at(232, 251, 12), 119);
    }

    #[test]
    fn test_block_at_edge() {
        let grid = Grid::new(0);
        assert_eq!(
            grid.block_at(Grid::WIDTH, Grid::HEIGHT, 3),
            grid.at(Grid::WIDTH, Grid::HEIGHT)
        );
        assert_eq!(grid.block_at(Grid::WIDTH + 1, Grid::HEIGHT + 1, 3), 0);
        assert_eq!(
            grid.block_at(Grid::WIDTH - 1, Grid::HEIGHT, 3),
            grid.at(Grid::WIDTH - 1, Grid::HEIGHT) + grid.at(Grid::WIDTH, Grid::HEIGHT)
        );
    }
}
