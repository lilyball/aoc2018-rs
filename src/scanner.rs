#![allow(unused)]

use lazy_static::lazy_static;
use regex::{self, Regex};

pub struct Scanner<'a> {
    source: &'a str,
    remainder: &'a str,
    chars_to_skip: Option<&'a dyn Fn(char) -> bool>,
}

impl<'a> Scanner<'a> {
    pub fn new(input: &'a str) -> Scanner<'a> {
        Scanner {
            source: input,
            remainder: input,
            chars_to_skip: None,
        }
    }

    /// Returns the source string.
    pub fn source(&self) -> &'a str {
        self.source
    }

    /// Returns the unparsed portion of the source string.
    pub fn remainder(&self) -> &'a str {
        self.remainder
    }

    /// Returns the closure that defines what characters will be skipped to satisfy the requested
    /// parse.
    pub fn chars_to_skip(&self) -> Option<&'a dyn Fn(char) -> bool> {
        self.chars_to_skip
    }

    /// Sets the characters that will be skipped to satisfy the requested parse.
    ///
    /// If the requested parse can be satisfied at the current position, it will be, even if the
    /// current characters could be skipped.
    pub fn set_chars_to_skip(&mut self, f: Option<&'a dyn Fn(char) -> bool>) {
        self.chars_to_skip = f;
    }

    /// Returns the current offset into the source string.
    pub fn offset(&self) -> usize {
        self.source.len() - self.remainder.len()
    }

    /// Sets the current offset into the source string.
    ///
    /// If the offset is too large, it will be set to the largest valid offset instead.
    ///
    /// Panics if the offset does not lie on a character boundary.
    pub fn set_offset(&mut self, offset: usize) {
        let offset = offset.min(self.source.len());
        assert!(
            self.source.is_char_boundary(offset),
            "Scanner.set_offset called with invalid offset"
        );
        self.remainder = &self.source[offset..];
    }

    /// Returns `true` if the scanner is at the end of the source string.
    ///
    /// This may return `true` when `remainder()` returns a non-empty string, if the remaining
    /// characters can all be skipped.
    pub fn is_at_end(&self) -> bool {
        if self.remainder.is_empty() {
            return true;
        }
        if let Some(ref f) = self.chars_to_skip {
            self.remainder.chars().all(|c| f(c))
        } else {
            false
        }
    }

    /// Scans characters that pass a given predicate.
    pub fn scan_while<F>(&mut self, mut f: F) -> &'a str
    where
        F: FnMut(char) -> bool,
    {
        let mut indices = self.remainder.char_indices();
        let (first_idx, first_char) = loop {
            match indices.next() {
                Some((idx, c)) => {
                    if f(c) {
                        break (idx, c);
                    }
                    match self.chars_to_skip {
                        Some(ref skip_f) if skip_f(c) => {}
                        _ => return "",
                    }
                }
                None => return "",
            }
        };
        match indices.find(|&(idx, c)| !f(c)) {
            Some((stop_idx, _)) => {
                let result = &self.remainder[first_idx..stop_idx];
                self.remainder = &self.remainder[stop_idx..];
                result
            }
            None => {
                let result = &self.remainder[first_idx..];
                self.remainder = "";
                result
            }
        }
    }

    /// Scans the given string, returning `true` if the string matched, otherwise `false`.
    pub fn scan_str(&mut self, needle: &str) -> bool {
        match self.chars_to_skip {
            Some(ref f) => {
                let mut remainder = self.remainder;
                while remainder.len() >= needle.len() {
                    if remainder.starts_with(needle) {
                        self.remainder = &remainder[needle.len()..];
                        return true;
                    }
                    let mut chars = remainder.chars();
                    let next_char = chars.next().unwrap();
                    if !f(next_char) {
                        break;
                    }
                    remainder = chars.as_str();
                }
                false
            }
            None => {
                if self.remainder.starts_with(needle) {
                    self.remainder = &self.remainder[needle.len()..];
                    true
                } else {
                    false
                }
            }
        }
    }

    /// Scans characters up to the given string and returns the scanned characters.
    /// If the given string is not present in the remainder, the entire remainder is scanned.
    pub fn scan_up_to_str(&mut self, needle: &str) -> &'a str {
        let (result, rest) = match self.remainder.find(needle) {
            Some(idx) => self.remainder.split_at(idx),
            None => (self.remainder, ""),
        };
        self.remainder = rest;
        result
    }

    /// Matches a regex at the current scanner offset and returns the matching text.
    ///
    /// The match is not performed taking the surrounding text into account. This means that e.g.
    /// the `\A` anchor will match even after you've scanned text.
    pub fn scan_regex(&mut self, regex: &Regex) -> Option<&'a str> {
        // NB: We don't take surrounding context into account even though we could here, with
        // `find_at`, because there's no equivalent `captures_at` method for `scan_regex_captures`.
        let re_match = regex.find(self.remainder)?;
        let (start, end) = (re_match.start(), re_match.end());
        if start != 0 {
            let f = self.chars_to_skip.as_ref()?;
            if self
                .remainder
                .char_indices()
                .take_while(|&(idx, _)| idx < start)
                .any(|(_, c)| !f(c))
            {
                return None;
            }
        }
        self.remainder = &self.remainder[end..];
        Some(re_match.as_str())
    }

    /// Matches a regex at the current scanner offset and returns the resulting captures.
    ///
    /// The match is not performed taking the surrounding text into account. This means that e.g.
    /// the `\A` anchor will match even after you've scanned text.
    pub fn scan_regex_captures(&mut self, regex: &Regex) -> Option<regex::Captures<'a>> {
        // NB: We don't take surrounding context into account because there's no `captures_at`
        // method.
        let captures = regex.captures(self.remainder)?;
        let re_match = captures.get(0)?; // this shouldn't fail
        let (start, end) = (re_match.start(), re_match.end());
        if start != 0 {
            let f = self.chars_to_skip.as_ref()?;
            if self
                .remainder
                .char_indices()
                .take_while(|&(idx, _)| idx < start)
                .any(|(_, c)| !f(c))
            {
                return None;
            }
        }
        self.remainder = &self.remainder[end..];
        Some(captures)
    }
}

pub trait Scannable
where
    Self: Sized,
{
    type Error;
    fn scan_from(scanner: &mut Scanner) -> Result<Self, Self::Error>;
}

impl<'a> Scanner<'a> {
    pub fn scan<T: Scannable>(&mut self) -> Result<T, T::Error> {
        T::scan_from(self)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ScanIntError {
    /// The scanner did not match an integer at the current position.
    NoMatch,
    /// The scanner matched an integer but it overflowed.
    Overflow,
    /// The scanner matched an integer but it underflowed.
    Underflow,
}

macro_rules! int_scanner {
    ($type:ty, $name:ident) => {
        impl<'a> Scanner<'a> {
            /// Scans the integral type.
            ///
            /// On overflow or underflow, the offset is unchanged.
            pub fn $name(&mut self) -> Result<$type, ScanIntError> {
                let offset = self.offset();
                let s = self.scan_regex(&RE_INT).ok_or(ScanIntError::NoMatch)?;
                s.parse::<$type>().or_else(|_| {
                    self.set_offset(offset);
                    if s.starts_with("-") {
                        Err(ScanIntError::Underflow)
                    } else {
                        Err(ScanIntError::Overflow)
                    }
                })
            }
        }

        impl Scannable for $type {
            type Error = ScanIntError;
            fn scan_from(scanner: &mut Scanner) -> Result<Self, Self::Error> {
                scanner.$name()
            }
        }
    };
}

lazy_static! {
    static ref RE_INT: Regex = Regex::new(r"-?\d+").unwrap();
}

int_scanner!(i8, scan_i8);
int_scanner!(i16, scan_i16);
int_scanner!(i32, scan_i32);
int_scanner!(i64, scan_i64);
int_scanner!(i128, scan_i128);
int_scanner!(isize, scan_isize);

macro_rules! uint_scanner {
    ($type:ty, $name:ident) => {
        impl<'a> Scanner<'a> {
            /// Scans the unsigned integral type.
            ///
            /// On overflow, the offset is unchanged.
            pub fn $name(&mut self) -> Result<$type, ScanIntError> {
                let offset = self.offset();
                let s = self.scan_while(|c| c.is_ascii_digit());
                if s.is_empty() {
                    Err(ScanIntError::NoMatch)
                } else {
                    s.parse::<$type>().or_else(|_| {
                        self.set_offset(offset);
                        Err(ScanIntError::Overflow)
                    })
                }
            }
        }

        impl Scannable for $type {
            type Error = ScanIntError;
            fn scan_from(scanner: &mut Scanner) -> Result<Self, Self::Error> {
                scanner.$name()
            }
        }
    };
}

uint_scanner!(u8, scan_u8);
uint_scanner!(u16, scan_u16);
uint_scanner!(u32, scan_u32);
uint_scanner!(u64, scan_u64);
uint_scanner!(u128, scan_u128);
uint_scanner!(usize, scan_usize);

lazy_static! {
    static ref RE_FLOAT: Regex = Regex::new(r"[-+]?(\d+(\.\d*)?(e-?\d+)?|\.\d+(e-?\d+)?)").unwrap();
}

impl<'a> Scanner<'a> {
    /// Scans a floating-point value.
    ///
    /// Does not scan `"inf"`, `"-inf"`, or `"NaN"`.
    ///
    /// Contains `inf` or `-inf` on overflow.
    pub fn scan_f32(&mut self) -> Option<f32> {
        self.scan_regex(&RE_FLOAT)?.parse::<f32>().ok()
    }

    /// Scans a floating-point value.
    ///
    /// Does not scan `"inf"`, `"-inf"`, or `"NaN"`.
    ///
    /// Contains `inf` or `-inf` on overflow.
    pub fn scan_f64(&mut self) -> Option<f64> {
        self.scan_regex(&RE_FLOAT)?.parse::<f64>().ok()
    }
}

impl Scannable for f32 {
    type Error = ();
    fn scan_from(scanner: &mut Scanner) -> Result<Self, Self::Error> {
        scanner.scan_f32().ok_or(())
    }
}

impl Scannable for f64 {
    type Error = ();
    fn scan_from(scanner: &mut Scanner) -> Result<Self, Self::Error> {
        scanner.scan_f64().ok_or(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn set_offset_and_remainder() {
        let mut scanner = Scanner::new("foobar");
        assert_eq!(scanner.offset(), 0);
        assert_eq!(scanner.remainder(), "foobar");
        scanner.set_offset(3);
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.remainder(), "bar");
        scanner.set_offset(10);
        assert_eq!(scanner.offset(), 6);
        assert_eq!(scanner.remainder(), "");
    }

    #[test]
    fn is_at_end() {
        let mut scanner = Scanner::new("foobar");
        assert!(!scanner.is_at_end());
        scanner.set_offset(5);
        assert!(!scanner.is_at_end());
        scanner.set_offset(6);
        assert!(scanner.is_at_end());
        scanner.set_offset(0);
        assert!(!scanner.is_at_end());
    }

    #[test]
    fn is_at_end_chars_to_skip() {
        let mut scanner = Scanner::new("foo bar  ");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert!(!scanner.is_at_end());
        scanner.set_offset(2);
        assert!(!scanner.is_at_end());
        scanner.set_offset(3);
        assert!(!scanner.is_at_end());
        scanner.set_offset(7);
        assert!(scanner.is_at_end());
        scanner.set_offset(9);
        assert!(scanner.is_at_end());
    }

    #[test]
    fn scan_while() {
        let mut scanner = Scanner::new("foobar");
        assert_eq!(scanner.scan_while(|c| c == 'f'), "f");
        assert_eq!(scanner.offset(), 1);
        assert_eq!(scanner.scan_while(|_| false), "");
        assert_eq!(scanner.offset(), 1);
        assert_eq!(scanner.scan_while(|c| c == 'o'), "oo");
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.scan_while(|_| true), "bar");
        assert!(scanner.is_at_end());
    }

    #[test]
    fn scan_while_chars_to_skip() {
        let mut scanner = Scanner::new("fo o bar");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert_eq!(scanner.scan_while(|c| c != ' '), "fo");
        assert_eq!(scanner.offset(), 2);
        assert_eq!(scanner.scan_while(|c| c == 'o'), "o");
        assert_eq!(scanner.offset(), 4);
        assert_eq!(scanner.scan_while(|c| c == ' '), " ");
        assert_eq!(scanner.offset(), 5);

        scanner = Scanner::new("a aa aaa    a   ");
        scanner.set_chars_to_skip(Some(&|c| c.is_whitespace()));
        assert_eq!(scanner.scan_while(|c| c == 'a'), "a");
        assert_eq!(scanner.offset(), 1);
        assert_eq!(scanner.scan_while(|c| c == 'a'), "aa");
        assert_eq!(scanner.offset(), 4);
        assert_eq!(scanner.scan_while(|c| c == 'a'), "aaa");
        assert_eq!(scanner.offset(), 8);
        assert_eq!(scanner.scan_while(|c| c == 'a'), "a");
        assert_eq!(scanner.offset(), 13);

        scanner.set_offset(1);
        assert_eq!(scanner.scan_while(|c| c == 'b'), "");
        assert_eq!(scanner.offset(), 1);
    }

    #[test]
    fn scan_str() {
        let mut scanner = Scanner::new("foobar");
        assert!(scanner.scan_str(""));
        assert_eq!(scanner.offset(), 0);
        assert!(scanner.scan_str("foo"));
        assert_eq!(scanner.offset(), 3);
        assert!(!scanner.scan_str("foo"));
        assert!(scanner.scan_str("bar"));
        assert_eq!(scanner.offset(), 6);
    }

    #[test]
    fn scan_str_chars_to_skip() {
        let mut scanner = Scanner::new("fo o bar");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert!(scanner.scan_str("fo"));
        assert_eq!(scanner.offset(), 2);
        assert!(scanner.scan_str(""));
        assert_eq!(scanner.offset(), 2);
        assert!(scanner.scan_str("o"));
        assert_eq!(scanner.offset(), 4);
        assert!(!scanner.scan_str("foo"));
        assert_eq!(scanner.offset(), 4);
        assert!(scanner.scan_str(" "));
        assert_eq!(scanner.offset(), 5);
    }

    #[test]
    fn scan_up_to_str() {
        let mut scanner = Scanner::new("foobar");
        assert_eq!(scanner.scan_up_to_str("foo"), "");
        assert_eq!(scanner.offset(), 0);
        assert_eq!(scanner.scan_up_to_str("bar"), "foo");
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.scan_up_to_str("flubber"), "bar");
        assert!(scanner.is_at_end());
    }

    #[test]
    fn scan_regex() {
        let mut scanner = Scanner::new("foobar");
        assert_eq!(
            scanner.scan_regex(&Regex::new(".{3}").unwrap()),
            Some("foo")
        );
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.scan_regex(&Regex::new("ba$").unwrap()), None);
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.scan_regex(&Regex::new("^").unwrap()), Some(""));
        assert_eq!(scanner.offset(), 3);
        assert_eq!(scanner.scan_regex(&Regex::new(".+").unwrap()), Some("bar"));
        assert_eq!(scanner.offset(), 6);
        assert_eq!(scanner.scan_regex(&Regex::new(".+").unwrap()), None);
        assert_eq!(scanner.offset(), 6);
    }

    #[test]
    fn scan_regex_chars_to_skip() {
        let mut scanner = Scanner::new("fo o bar");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert_eq!(scanner.scan_regex(&Regex::new("f?o+").unwrap()), Some("fo"));
        assert_eq!(scanner.offset(), 2);
        assert_eq!(scanner.scan_regex(&Regex::new("f?o+").unwrap()), Some("o"));
        assert_eq!(scanner.offset(), 4);
        assert_eq!(scanner.scan_regex(&Regex::new("baz").unwrap()), None);
        assert_eq!(scanner.offset(), 4);
    }

    #[test]
    fn scan_regex_captures() {
        let mut scanner = Scanner::new("foobar");
        assert_eq!(
            scanner
                .scan_regex_captures(&Regex::new("f(o+)").unwrap())
                .map(|c| c.get(1).unwrap().as_str()),
            Some("oo")
        );
        assert_eq!(scanner.offset(), 3);
    }

    #[test]
    fn scan_regex_captures_chars_to_skip() {
        let mut scanner = Scanner::new("fo o  bar");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        let re = Regex::new("f?(o+)").unwrap();
        assert_eq!(
            scanner
                .scan_regex_captures(&re)
                .map(|c| (c.get(0).unwrap().as_str(), c.get(1).unwrap().as_str())),
            Some(("fo", "o"))
        );
        assert_eq!(scanner.offset(), 2);
        assert_eq!(
            scanner
                .scan_regex_captures(&re)
                .map(|c| (c.get(0).unwrap().as_str(), c.get(1).unwrap().as_str())),
            Some(("o", "o"))
        );
        assert_eq!(scanner.offset(), 4);
        assert!(scanner.scan_regex_captures(&re).is_none());
        assert_eq!(scanner.offset(), 4);
    }

    #[test]
    fn scan_i32() {
        let mut scanner = Scanner::new("foo 123-321 bar");
        assert_eq!(scanner.scan_i32(), Err(ScanIntError::NoMatch));
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(4);
        assert_eq!(scanner.scan_i32(), Ok(123));
        assert_eq!(scanner.offset(), 7);
        assert_eq!(scanner.scan_i32(), Ok(-321));
        assert_eq!(scanner.offset(), 11);
    }

    #[test]
    fn scan_i32_overflow() {
        let mut scanner = Scanner::new("foo 3285718937581723 -230982093850 bar");
        scanner.set_offset(4);
        assert_eq!(scanner.scan_i32(), Err(ScanIntError::Overflow));
        assert_eq!(scanner.offset(), 4);
        scanner.set_offset(21);
        assert_eq!(scanner.scan_i32(), Err(ScanIntError::Underflow));
        assert_eq!(scanner.offset(), 21);
    }

    #[test]
    fn scan_i32_chars_to_skip() {
        let mut scanner = Scanner::new("foo 123 -321");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert_eq!(scanner.scan_i32(), Err(ScanIntError::NoMatch));
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(3);
        assert_eq!(scanner.scan_i32(), Ok(123));
        assert_eq!(scanner.offset(), 7);
        assert_eq!(scanner.scan_i32(), Ok(-321));
    }

    #[test]
    fn scan_i32_overflow_chars_to_skip() {
        let mut scanner = Scanner::new("foo 1239581203581092385");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        scanner.set_offset(3);
        assert_eq!(scanner.scan_i32(), Err(ScanIntError::Overflow));
        assert_eq!(scanner.offset(), 3);
    }

    #[test]
    fn scan_u32() {
        let mut scanner = Scanner::new("foo 123-321 bar");
        assert_eq!(scanner.scan_u32(), Err(ScanIntError::NoMatch));
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(4);
        assert_eq!(scanner.scan_u32(), Ok(123));
        assert_eq!(scanner.offset(), 7);
        assert_eq!(scanner.scan_u32(), Err(ScanIntError::NoMatch));
        assert_eq!(scanner.offset(), 7);
    }

    #[test]
    fn scan_u32_overflow() {
        let mut scanner = Scanner::new("foo 3285718937581723 bar");
        scanner.set_offset(4);
        assert_eq!(scanner.scan_u32(), Err(ScanIntError::Overflow));
        assert_eq!(scanner.offset(), 4);
    }

    #[test]
    fn scan_u32_chars_to_skip() {
        let mut scanner = Scanner::new("foo 123   321");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert_eq!(scanner.scan_u32(), Err(ScanIntError::NoMatch));
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(3);
        assert_eq!(scanner.scan_u32(), Ok(123));
        assert_eq!(scanner.offset(), 7);
        assert_eq!(scanner.scan_u32(), Ok(321));
    }

    #[test]
    fn scan_u32_overflow_chars_to_skip() {
        let mut scanner = Scanner::new("foo 1239581203581092385");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        scanner.set_offset(3);
        assert_eq!(scanner.scan_u32(), Err(ScanIntError::Overflow));
        assert_eq!(scanner.offset(), 3);
    }

    #[test]
    fn scan_f32() {
        let mut scanner = Scanner::new("foo 1 2.34 -12.e-10 12e bar");
        assert_eq!(scanner.scan_f32(), None);
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(4);
        assert_eq!(scanner.scan_f32(), Some(1.0));
        assert_eq!(scanner.offset(), 5);
        assert!(scanner.scan_str(" "));
        assert_eq!(scanner.scan_f32(), Some(2.34));
        assert_eq!(scanner.offset(), 10);
        assert!(scanner.scan_str(" "));
        assert_eq!(scanner.scan_f32(), Some(-12e-10));
        assert_eq!(scanner.offset(), 19);
        assert!(scanner.scan_str(" "));
        assert_eq!(scanner.scan_f32(), Some(12.0));
        assert_eq!(scanner.offset(), 22);
        assert!(scanner.scan_str("e"));
    }

    #[test]
    fn scan_f32_chars_to_skip() {
        let mut scanner = Scanner::new("foo 1 2.34 -12.e-10 12e bar");
        scanner.set_chars_to_skip(Some(&char::is_whitespace));
        assert_eq!(scanner.scan_f32(), None);
        assert_eq!(scanner.offset(), 0);
        scanner.set_offset(4);
        assert_eq!(scanner.scan_f32(), Some(1.0));
        assert_eq!(scanner.offset(), 5);
        assert_eq!(scanner.scan_f32(), Some(2.34));
        assert_eq!(scanner.offset(), 10);
        assert_eq!(scanner.scan_f32(), Some(-12e-10));
        assert_eq!(scanner.offset(), 19);
        assert_eq!(scanner.scan_f32(), Some(12.0));
        assert_eq!(scanner.offset(), 22);
        assert!(scanner.scan_str("e"));
    }
}
