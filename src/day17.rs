use crate::grid::*;
use crate::helpers::*;
use crate::scanner::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::ops::Range;

#[derive(Debug, Clone, PartialEq, Eq)]
struct Coordinate {
    x: Range<i32>,
    y: Range<i32>,
}

impl Coordinate {
    fn from_str(input: &str) -> Coordinate {
        let mut scanner = Scanner::new(input);
        if scanner.scan_str("x=") {
            let x = scanner.scan().expect("missing x value");
            assert!(scanner.scan_str(", y="));
            let y_start = scanner.scan().expect("missing y start value");
            assert!(scanner.scan_str(".."));
            let y_end = scanner.scan_i32().expect("missing y end value");
            assert!(scanner.is_at_end());
            Coordinate {
                x: x..(x + 1),
                y: y_start..(y_end + 1),
            }
        } else {
            assert!(scanner.scan_str("y="));
            let y = scanner.scan().expect("missing y value");
            assert!(scanner.scan_str(", x="));
            let x_start = scanner.scan().expect("missing x start value");
            assert!(scanner.scan_str(".."));
            let x_end = scanner.scan_i32().expect("missing x end value");
            assert!(scanner.is_at_end());
            Coordinate {
                x: x_start..(x_end + 1),
                y: y..(y + 1),
            }
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Tile {
    Sand,
    WetSand,
    Clay,
    Water,
}

#[derive(Clone)]
struct Map {
    tiles: Grid<Tile>,
    origin: Coord, // the coordinate of the upper-left tile
}

impl Map {
    const SPRING_LOC: Coord = Coord::new(500, 0);

    fn from_str(input: &str) -> Map {
        let coords = input.lines().map(Coordinate::from_str).collect::<Vec<_>>();
        let x_range = {
            let mut coords = coords.iter().map(|c| c.x.clone());
            let first = coords.next().unwrap();
            coords.fold(first, |a, b| a.union(&b))
        };
        // Expand the x-range by 1 in both directions, so water can't fall off the sides.
        // This is important as any water that would fall off the sides needs to be counted.
        let x_range = (x_range.start - 1)..(x_range.end + 1);
        let y_range = {
            let mut coords = coords.iter().map(|c| c.y.clone());
            let first = coords.next().unwrap();
            coords.fold(first, |a, b| a.union(&b))
        };
        let origin = Coord::new(x_range.start, y_range.start);
        let mut tiles = Grid::new_clone(
            Size::new(x_range.len() as i32, y_range.len() as i32),
            &Tile::Sand,
        );
        for coord in coords {
            for y in coord.y {
                for x in coord.x.clone() {
                    tiles[Coord::new(x, y) - origin] = Tile::Clay;
                }
            }
        }
        Map { tiles, origin }
    }

    fn get(&self, at: Coord) -> Option<&Tile> {
        self.tiles.get(at - self.origin)
    }

    #[allow(unused)]
    fn get_mut(&mut self, at: Coord) -> Option<&mut Tile> {
        self.tiles.get_mut(at - self.origin)
    }

    /// Spawns a tile of water.
    /// Returns `true` if the water made changes to the map, otherwise `false`.
    fn spawn_water(&mut self) -> bool {
        let mut water_coord = Self::SPRING_LOC;
        water_coord.y = water_coord.y.max(self.origin.y);
        assert!(
            water_coord.x >= self.origin.x
                && water_coord.x - self.origin.x < self.tiles.size().width,
            "spring doesn't affect the map"
        );
        let mut pending_coords = vec![water_coord];
        let mut modified = false;
        'outer: while let Some(mut water_coord) = pending_coords.pop() {
            while let Some(&tile) = self.get(water_coord) {
                match tile {
                    Tile::Sand => {
                        self[water_coord] = Tile::WetSand;
                        modified = true;
                        water_coord.y += 1;
                        continue;
                    }
                    Tile::WetSand => {
                        water_coord.y += 1;
                        continue;
                    }
                    Tile::Water => {
                        // Check if we can spread sideways from the water
                        for offset in &[Coord::new(-1, 0), Coord::new(1, 0)] {
                            let mut coord = water_coord;
                            loop {
                                coord += offset;
                                match self.get(coord) {
                                    None => {
                                        // We fell off the map
                                        panic!("fell sideways off the map");
                                    }
                                    Some(&Tile::Water) => {}
                                    Some(&Tile::Sand) | Some(&Tile::WetSand) => {
                                        // Place the water here
                                        self[coord] = Tile::Water;
                                        modified = true;
                                        continue 'outer;
                                    }
                                    Some(&Tile::Clay) => break,
                                }
                            }
                        }
                        // We ran into clay on both sides
                    }
                    Tile::Clay => {}
                }
                // Spill up
                water_coord.y -= 1;
                // Wet the sand in both directions
                let mut fall_coord = None;
                for offset in &[Coord::new(-1, 0), Coord::new(1, 0)] {
                    let mut coord = water_coord;
                    loop {
                        coord += offset;
                        match self.get(coord) {
                            None => panic!("fell sideways off the map"),
                            Some(&Tile::Water) => {
                                // We must have spilled in two spots from a higher basin
                                break;
                            }
                            Some(&Tile::Sand) => {
                                self[coord] = Tile::WetSand;
                                modified = true;
                            }
                            Some(&Tile::WetSand) => {}
                            Some(&Tile::Clay) => break,
                        }
                        match self[coord + Coord::new(0, 1)] {
                            Tile::Sand | Tile::WetSand => {
                                if let Some(old_coord) = fall_coord {
                                    pending_coords.push(old_coord);
                                }
                                fall_coord = Some(coord);
                                break;
                            }
                            Tile::Clay | Tile::Water => {}
                        }
                    }
                }
                if let Some(coord) = fall_coord {
                    water_coord = coord;
                } else {
                    self[water_coord] = Tile::Water;
                    modified = true;
                    continue 'outer;
                }
            }
        }
        modified
    }

    fn spawn_all_water(&mut self) {
        while self.spawn_water() {}
    }

    fn count_tiles_of_type(&self, tile: Tile) -> usize {
        self.tiles
            .iter()
            .filter(|(_, &tile_)| tile == tile_)
            .count()
    }
}

impl std::ops::Index<Coord> for Map {
    type Output = Tile;
    fn index(&self, index: Coord) -> &Tile {
        &self.tiles[index - self.origin]
    }
}

impl std::ops::IndexMut<Coord> for Map {
    fn index_mut(&mut self, index: Coord) -> &mut Tile {
        &mut self.tiles[index - self.origin]
    }
}

impl std::fmt::Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use std::fmt::Write;
        for (coord, tile) in self.tiles.iter() {
            if coord.x == 0 && coord.y != 0 {
                f.write_char('\n')?;
            }
            let c = match tile {
                Tile::Sand => '.',
                Tile::WetSand => '|',
                Tile::Clay => '#',
                Tile::Water => '~',
            };
            f.write_char(c)?;
        }
        Ok(())
    }
}

#[aoc_generator(day17)]
fn input_generator(input: &str) -> Map {
    Map::from_str(input)
}

#[aoc(day17, part1)]
fn part1(input: &Map) -> usize {
    let mut map = input.clone();
    map.spawn_all_water();
    map.count_tiles_of_type(Tile::Water) + map.count_tiles_of_type(Tile::WetSand)
}

#[aoc(day17, part2)]
fn part2(input: &Map) -> usize {
    let mut map = input.clone();
    map.spawn_all_water();
    map.count_tiles_of_type(Tile::Water)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn coordinate_from_str() {
        assert_eq!(
            Coordinate::from_str("y=1181, x=576..579"),
            Coordinate {
                x: 576..580,
                y: 1181..1182
            }
        );
        assert_eq!(
            Coordinate::from_str("x=647, y=653..675"),
            Coordinate {
                x: 647..648,
                y: 653..676
            }
        );
    }

    #[test]
    fn map_from_str() {
        let map = Map::from_str(include_str!("../test_data/day17.txt"));
        assert_eq!(map.origin, Coord::new(494, 1));
        assert_eq!(map.tiles.size(), Size::new(14, 13));
        assert_eq!(map[Coord::new(495, 2)], Tile::Clay);
        assert_eq!(map[Coord::new(500, 5)], Tile::Sand);
        assert_eq!(map[Coord::new(500, 7)], Tile::Clay);
        assert_eq!(map[Coord::new(504, 13)], Tile::Clay);
    }

    #[test]
    fn map_spawn_water() {
        let mut map = Map::from_str(include_str!("../test_data/day17.txt"));
        assert!(map.spawn_water());
        assert_eq!(map[Coord::new(500, 6)], Tile::Water);
        assert_eq!(map[Coord::new(500, 5)], Tile::WetSand);
        assert_eq!(map[Coord::new(499, 6)], Tile::WetSand);
        assert!(map.spawn_water());
        assert_eq!(map[Coord::new(499, 6)], Tile::Water);
        assert_eq!(map[Coord::new(498, 6)], Tile::WetSand);
        for i in 0..3 {
            assert!(map.spawn_water());
            let coord = Coord::new(498 - i, 6);
            assert_eq!(map[coord], Tile::Water, "coord {}", coord);
        }
        assert_eq!(map[Coord::new(500, 5)], Tile::WetSand);
        assert_eq!(map[Coord::new(499, 5)], Tile::Sand);
        for i in 0..5 {
            assert!(map.spawn_water());
            let coord = Coord::new(500 - i, 5);
            assert_eq!(map[coord], Tile::Water, "coord {}", coord);
        }
        for i in 0..2 {
            assert!(map.spawn_water());
            let coord = Coord::new(500 - i, 4);
            assert_eq!(map[coord], Tile::Water, "coord {}", coord);
        }
        for i in 0..2 {
            assert!(map.spawn_water());
            let coord = Coord::new(500 - i, 3);
            assert_eq!(map[coord], Tile::Water, "coord {}", coord);
            let coord = Coord::new(497 - i, 4);
            assert_eq!(map[coord], Tile::Sand, "coord {}", coord);
        }
        // spillover
        assert!(map.spawn_water());
        for x in 499..=502 {
            let coord = Coord::new(x, 2);
            assert_eq!(map[coord], Tile::WetSand, "coord {}", coord);
        }
        for y in 3..=11 {
            let coord = Coord::new(502, y);
            assert_eq!(map[coord], Tile::WetSand, "coord {}", coord);
        }
        assert_eq!(map[Coord::new(502, 12)], Tile::Water);
        for _ in 0..14 {
            assert!(map.spawn_water()); // fill the second basin
        }
        // spillover again
        assert!(map.spawn_water());
        for x in 497..=505 {
            let coord = Coord::new(x, 9);
            assert_eq!(map[coord], Tile::WetSand, "coord {}", coord);
        }
        for y in 10..=13 {
            let coord = Coord::new(497, y);
            assert_eq!(map[coord], Tile::WetSand, "coord {}", coord);
            let coord = Coord::new(505, y);
            assert_eq!(map[coord], Tile::WetSand, "coord {}", coord);
        }
        // no more changes
        assert!(!map.spawn_water());
        assert_eq!(
            map.count_tiles_of_type(Tile::Water) + map.count_tiles_of_type(Tile::WetSand),
            57
        );
    }
}
