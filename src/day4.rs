use aoc_runner_derive::{aoc, aoc_generator};
use chrono::prelude::*;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::ops::Range;
use std::str::FromStr;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Entry {
    timestamp: DateTime<Utc>,
    action: EntryAction,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum EntryAction {
    BeginShift(i32),
    FallAsleep,
    WakeUp,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum EntryParseError {
    ChronoParseError(chrono::format::ParseError),
    InvalidFormat,
}

impl From<chrono::format::ParseError> for EntryParseError {
    fn from(err: chrono::format::ParseError) -> Self {
        EntryParseError::ChronoParseError(err)
    }
}

impl FromStr for Entry {
    type Err = EntryParseError;
    // Parses strings like
    // [1518-11-01 00:00] Guard #10 begins shift
    // [1518-11-01 00:05] falls asleep
    // [1518-11-01 00:25] wakes up
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"^\[([^]]+)\] (?:Guard #(\d+) begins shift|(falls asleep)|(wakes up))$"
            )
            .unwrap();
        }
        let caps = RE.captures(s).ok_or(EntryParseError::InvalidFormat)?;
        let timestamp_str = caps.get(1).unwrap().as_str();
        let timestamp = Utc.datetime_from_str(timestamp_str, "%Y-%m-%d %H:%M")?;
        let action: EntryAction;
        if let Some(guard_str) = caps.get(2).map(|c| c.as_str()) {
            let guard =
                i32::from_str_radix(guard_str, 10).or(Err(EntryParseError::InvalidFormat))?;
            action = EntryAction::BeginShift(guard);
        } else if caps.get(3).is_some() {
            action = EntryAction::FallAsleep;
        } else if caps.get(4).is_some() {
            action = EntryAction::WakeUp;
        } else {
            return Err(EntryParseError::InvalidFormat);
        }
        Ok(Entry { timestamp, action })
    }
}

type Nap = Range<DateTime<Utc>>;

#[derive(Debug, Clone, PartialEq, Eq)]
struct Shift {
    guard: i32,
    begin: DateTime<Utc>,
    naps: Vec<Nap>,
}

impl Shift {
    fn total_nap_minutes(&self) -> i64 {
        self.naps
            .iter()
            .map(|nap| nap.end.signed_duration_since(nap.start).num_minutes())
            .sum()
    }
}

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Vec<Shift> {
    let mut entries = input
        .lines()
        .map(|l| Entry::from_str(l).unwrap())
        .collect::<Vec<_>>();
    entries.sort_unstable_by_key(|e| e.timestamp);
    let first_shift = match entries[0] {
        Entry {
            timestamp,
            action: EntryAction::BeginShift(guard),
        } => Shift {
            guard,
            begin: timestamp,
            naps: Vec::new(),
        },
        _ => panic!("entries didn't begin with a shift"),
    };
    let (mut shifts, shift, nap_start) = entries.into_iter().skip(1).fold(
        (Vec::new(), first_shift, None),
        |(mut shifts, mut shift, nap_start), entry| match entry.action {
            EntryAction::BeginShift(guard) => {
                assert!(nap_start.is_none(), "guard ended shift while asleep");
                shifts.push(shift);
                let new_shift = Shift {
                    guard,
                    begin: entry.timestamp,
                    naps: Vec::new(),
                };
                (shifts, new_shift, None)
            }
            EntryAction::FallAsleep => {
                assert!(nap_start.is_none(), "overlapping naps");
                (shifts, shift, Some(entry.timestamp))
            }
            EntryAction::WakeUp => {
                let nap_start = nap_start.expect("guard didn't start nap");
                shift.naps.push(nap_start..entry.timestamp);
                (shifts, shift, None)
            }
        },
    );
    assert!(nap_start.is_none(), "last guard didn't finish nap");
    shifts.push(shift);
    shifts
}

#[aoc(day4, part1)]
fn part1<'a>(input: &'a [Shift]) -> i32 {
    let mut guards = HashMap::<i32, Vec<&'a Shift>>::new();
    for shift in input {
        guards.entry(shift.guard).or_default().push(shift);
    }
    let sleepiest_guard = *guards
        .iter()
        .map(|(guard, shifts)| {
            let total_nap_minutes = shifts
                .iter()
                .map(|shift| shift.total_nap_minutes())
                .sum::<i64>();
            (guard, total_nap_minutes)
        })
        .max_by_key(|&(_, nap_time)| nap_time)
        .unwrap()
        .0;
    let mut minutes = [0i32; 60];
    for shift in guards.get(&sleepiest_guard).unwrap() {
        for nap in &shift.naps {
            for minute in nap.start.time().minute()..nap.end.time().minute() {
                minutes[minute as usize] += 1;
            }
        }
    }
    let best_minute = minutes
        .iter()
        .enumerate()
        .max_by_key(|(_, &m)| m)
        .unwrap()
        .0 as i32;
    sleepiest_guard * best_minute
}

#[aoc(day4, part2)]
fn part2<'a>(input: &'a [Shift]) -> i32 {
    let mut guards = HashMap::<i32, Vec<&'a Shift>>::new();
    for shift in input {
        guards.entry(shift.guard).or_default().push(shift);
    }
    let (guard, best_minute, _) = guards
        .into_iter()
        .map(|(guard, shifts)| {
            let mut minutes = [0; 60];
            for shift in shifts {
                for nap in &shift.naps {
                    for minute in nap.start.time().minute()..nap.end.time().minute() {
                        minutes[minute as usize] += 1;
                    }
                }
            }
            let (best_minute, count) = minutes.iter().enumerate().max_by_key(|(_, &m)| m).unwrap();
            (guard, best_minute as i32, *count)
        })
        .max_by_key(|&(_, _, count)| count)
        .unwrap();
    guard * best_minute
}
