use aoc_runner_derive::{aoc, aoc_generator};
use regex::Regex;

#[derive(Debug, Clone)]
struct Node {
    children: Vec<Node>,
    metadata: Vec<usize>,
}

impl Node {
    fn parse<I>(iter: &mut I) -> Node
    where
        I: Iterator<Item = usize>,
    {
        let num_children = iter.next().unwrap();
        let num_metadata = iter.next().unwrap();
        let children = std::iter::repeat_with(|| Node::parse(iter))
            .take(num_children)
            .collect();
        let metadata = iter.take(num_metadata).collect();
        Node { children, metadata }
    }

    fn sum_metadata(&self) -> usize {
        self.metadata.iter().cloned().sum::<usize>()
            + self
                .children
                .iter()
                .map(|n| n.sum_metadata())
                .sum::<usize>()
    }

    fn node_value(&self) -> usize {
        if self.children.is_empty() {
            self.sum_metadata()
        } else {
            self.metadata
                .iter()
                .cloned()
                .filter(|&x| x > 0)
                .flat_map(|x| self.children.get(x - 1))
                .map(|n| n.node_value())
                .sum()
        }
    }
}

#[aoc_generator(day8)]
fn input_generator(input: &str) -> Node {
    let regex = Regex::new(r#"\d+"#).unwrap();
    let mut input_iter = regex
        .find_iter(input)
        .map(|m| m.as_str().parse::<usize>().unwrap());
    Node::parse(&mut input_iter)
}

#[aoc(day8, part1)]
fn part1(input: &Node) -> usize {
    input.sum_metadata()
}

#[aoc(day8, part2)]
fn part2(input: &Node) -> usize {
    input.node_value()
}
