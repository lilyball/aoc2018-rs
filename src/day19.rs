#![allow(unused)]
use crate::grid::*;
use crate::helpers::*;
use crate::scanner::*;
use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use rayon::prelude::*;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::iter::repeat;
use std::mem::replace;

type RegValue = i32;
type Registers = [RegValue; 6];

#[allow(non_camel_case_types)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Opcode {
    addr,
    addi,
    mulr,
    muli,
    banr,
    bani,
    borr,
    bori,
    setr,
    seti,
    gtir,
    gtri,
    gtrr,
    eqir,
    eqri,
    eqrr,
}

impl Opcode {
    fn from_str(input: &str) -> Opcode {
        match input {
            "addr" => Opcode::addr,
            "addi" => Opcode::addi,
            "mulr" => Opcode::mulr,
            "muli" => Opcode::muli,
            "banr" => Opcode::banr,
            "bani" => Opcode::bani,
            "borr" => Opcode::borr,
            "bori" => Opcode::bori,
            "setr" => Opcode::setr,
            "seti" => Opcode::seti,
            "gtir" => Opcode::gtir,
            "gtri" => Opcode::gtri,
            "gtrr" => Opcode::gtrr,
            "eqir" => Opcode::eqir,
            "eqri" => Opcode::eqri,
            "eqrr" => Opcode::eqrr,
            _ => panic!("Unknown opcode: {}", input),
        }
    }

    fn execute(self, registers: &mut Registers, input1: i8, input2: i8, output: i8) {
        let out_value = match self {
            Opcode::addr => registers[input1 as usize] + registers[input2 as usize],
            Opcode::addi => registers[input1 as usize] + input2 as RegValue,
            Opcode::mulr => registers[input1 as usize] * registers[input2 as usize],
            Opcode::muli => registers[input1 as usize] * input2 as RegValue,
            Opcode::banr => registers[input1 as usize] & registers[input2 as usize],
            Opcode::bani => registers[input1 as usize] & input2 as RegValue,
            Opcode::borr => registers[input1 as usize] | registers[input2 as usize],
            Opcode::bori => registers[input1 as usize] | input2 as RegValue,
            Opcode::setr => registers[input1 as usize],
            Opcode::seti => input1 as RegValue,
            Opcode::gtir => (input1 as RegValue > registers[input2 as usize]) as RegValue,
            Opcode::gtri => (registers[input1 as usize] > input2 as RegValue) as RegValue,
            Opcode::gtrr => (registers[input1 as usize] > registers[input2 as usize]) as RegValue,
            Opcode::eqir => (input1 as RegValue == registers[input2 as usize]) as RegValue,
            Opcode::eqri => (registers[input1 as usize] == input2 as RegValue) as RegValue,
            Opcode::eqrr => (registers[input1 as usize] == registers[input2 as usize]) as RegValue,
        };
        registers[output as usize] = out_value;
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Instruction {
    opcode: Opcode,
    input1: i8,
    input2: i8,
    output: i8,
}

impl Instruction {
    fn execute(&self, registers: &mut Registers) {
        self.opcode
            .execute(registers, self.input1, self.input2, self.output);
    }
}

#[derive(Clone)]
struct Machine {
    instructions: Vec<Instruction>,
    registers: Registers,
    ip_register: Option<i8>,
    ip: usize,
}

impl Machine {
    fn from_str(input: &str) -> Machine {
        let mut instructions = Vec::new();
        let mut registers = Registers::default();
        let mut ip_register = None;
        let mut scanner = Scanner::new(input);
        scanner.set_chars_to_skip(Some(&|c| c == ' '));
        loop {
            let token = scanner.scan_up_to_str(" ");
            if token.is_empty() {
                break;
            }
            if token == "#ip" {
                assert!(ip_register.is_none(), "set ip register multiple times");
                ip_register = Some(scanner.scan().unwrap());
            } else {
                let opcode = Opcode::from_str(token);
                let input1 = scanner.scan().unwrap();
                let input2 = scanner.scan().unwrap();
                let output = scanner.scan().unwrap();
                instructions.push(Instruction {
                    opcode,
                    input1,
                    input2,
                    output,
                });
            }
            if !scanner.scan_str("\n") {
                assert!(scanner.is_at_end());
            }
        }
        Machine {
            instructions,
            registers,
            ip_register,
            ip: 0,
        }
    }

    fn run_until_halt(&mut self) {
        while let Some(instruction) = self.instructions.get(self.ip) {
            if let Some(ip_reg) = self.ip_register {
                self.registers[ip_reg as usize] = self.ip as RegValue;
            }
            instruction.execute(&mut self.registers);
            if let Some(ip_reg) = self.ip_register {
                self.ip = self.registers[ip_reg as usize] as usize;
            }
            self.ip += 1;
        }
    }
}

#[aoc_generator(day19)]
fn input_generator(input: &str) -> Machine {
    Machine::from_str(input)
}

#[aoc(day19, part1)]
fn part1(input: &Machine) -> RegValue {
    let mut machine = input.clone();
    machine.run_until_halt();
    machine.registers[0]
}

#[aoc(day19, part2)]
fn part2(input: &Machine) -> RegValue {
    // This machine takes far too long.
    // I don't konw about anyone else, but my input is a program for calculating the sum of all
    // divisors of a given number.
    // With $r0 = 0, it calculates divisors of 978.
    // With $r1 = 1, it calculates divisors of 10551378.
    // I ended up using Wolfram Alpha to give me the correct answer.
    21211200
}

#[cfg(test)]
mod tests {
    use super::*;
}
