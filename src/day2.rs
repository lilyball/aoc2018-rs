use aoc_runner_derive::aoc;
use std::collections::HashMap;
use strsim::hamming;

#[aoc(day2, part1)]
fn part1(input: &str) -> usize {
    let counts = input.lines().map(count_chars).collect::<Vec<_>>();
    let exactly_two = counts
        .iter()
        .filter(|c| c.values().any(|&c| c == 2))
        .count();
    let exactly_three = counts
        .into_iter()
        .filter(|c| c.values().any(|&c| c == 3))
        .count();
    exactly_two * exactly_three
}

fn count_chars(input: &str) -> HashMap<char, i32> {
    let mut map = HashMap::new();
    for c in input.chars() {
        *map.entry(c).or_default() += 1;
    }
    map
}

#[aoc(day2, part2)]
fn part2(input: &str) -> String {
    let pair = input
        .lines()
        .into_iter()
        .find_map(|line| {
            let line2 = input.lines().find(|line2| match hamming(line, line2) {
                Ok(1) => true,
                _ => false,
            })?;
            Some((line, line2))
        })
        .unwrap();
    pair.0
        .chars()
        .zip(pair.1.chars())
        .filter(|(a, b)| a == b)
        .map(|(a, _)| a)
        .collect()
}
