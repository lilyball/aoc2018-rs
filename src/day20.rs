use crate::grid::*;
use aoc_runner_derive::{aoc, aoc_generator};
use bitflags::bitflags;
use std::collections::HashMap;

bitflags! {
    #[derive(Default)]
    struct Directions: u8 {
        const N = 0b0001;
        const W = 0b0010;
        const E = 0b0100;
        const S = 0b1000;
    }
}

#[derive(Clone)]
struct State {
    tiles: Grid<Directions>,
    origin: Coord,
}

impl State {
    fn from_pattern(pattern: &str) -> State {
        assert!(pattern.starts_with("^"));
        assert!(pattern.ends_with("$"));
        let pattern = &pattern[1..(pattern.len() - 1)];
        let mut tiles_map = HashMap::<Coord, Directions>::new();
        let mut current = Coord::new(0, 0);
        let mut stack = Vec::new();
        for c in pattern.chars() {
            match c {
                'N' => {
                    *tiles_map.entry(current).or_default() |= Directions::N;
                    current.y -= 1;
                    *tiles_map.entry(current).or_default() |= Directions::S;
                }
                'W' => {
                    *tiles_map.entry(current).or_default() |= Directions::W;
                    current.x -= 1;
                    *tiles_map.entry(current).or_default() |= Directions::E;
                }
                'E' => {
                    *tiles_map.entry(current).or_default() |= Directions::E;
                    current.x += 1;
                    *tiles_map.entry(current).or_default() |= Directions::W;
                }
                'S' => {
                    *tiles_map.entry(current).or_default() |= Directions::S;
                    current.y += 1;
                    *tiles_map.entry(current).or_default() |= Directions::N;
                }
                '(' => {
                    stack.push(current);
                }
                '|' => {
                    current = *stack.last().expect("can't branch; empty stack");
                }
                ')' => {
                    stack.pop().expect("can't end group; empty stack");
                }
                _ => panic!("unknown pattern char {}", c),
            }
        }
        // bounds is (upper-left, lower-right)
        let bounds = tiles_map.keys().fold(
            (Coord::new(0, 0), Coord::new(0, 0)),
            |mut bounds, &coord| {
                bounds.0.x = bounds.0.x.min(coord.x);
                bounds.0.y = bounds.0.y.min(coord.y);
                bounds.1.x = bounds.1.x.max(coord.x);
                bounds.1.y = bounds.1.y.max(coord.y);
                bounds
            },
        );
        let origin = -bounds.0;
        let tiles = Grid::new_with(
            Size::new(bounds.1.x - bounds.0.x + 1, bounds.1.y - bounds.0.y + 1),
            |coord| {
                tiles_map
                    .get(&(coord - origin))
                    .map_or(Directions::empty(), |&x| x)
            },
        );
        State { tiles, origin }
    }

    /// Makes a cost map where each cell is the number of steps from the origin.
    fn make_cost_map(&self) -> Grid<Option<usize>> {
        let mut map = Grid::new_default(self.tiles.size());
        let mut cost = 0;
        map[self.origin] = Some(0);
        loop {
            let mut modified = false;
            let new_cost = cost + 1;
            for coord in map.iter_coords() {
                if map[coord] == Some(cost) {
                    let dirs = self.tiles[coord];
                    const STEPS: [(Directions, Coord); 4] = [
                        (Directions::N, Coord::new(0, -1)),
                        (Directions::W, Coord::new(-1, 0)),
                        (Directions::E, Coord::new(1, 0)),
                        (Directions::S, Coord::new(0, 1)),
                    ];
                    for &(dir, step) in &STEPS {
                        if dirs.contains(dir) {
                            if let Some(slot) = map.get_mut(coord + step) {
                                if slot.is_none() {
                                    *slot = Some(new_cost);
                                    modified = true;
                                }
                            }
                        }
                    }
                }
            }
            if !modified {
                break;
            }
            cost = new_cost;
        }
        map
    }
}

fn largest_cost(map: &Grid<Option<usize>>) -> Option<usize> {
    map.iter().filter_map(|(_, &x)| x).max()
}

#[aoc_generator(day20)]
fn input_generator(input: &str) -> State {
    State::from_pattern(input)
}

#[aoc(day20, part1)]
fn part1(input: &State) -> usize {
    let cost_map = input.make_cost_map();
    largest_cost(&cost_map).unwrap()
}

#[aoc(day20, part2)]
fn part2(input: &State) -> usize {
    let cost_map = input.make_cost_map();
    cost_map
        .iter()
        .filter_map(|(_, &cost)| cost)
        .filter(|&cost| cost >= 1000)
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_pattern_1() {
        let state = State::from_pattern("^WNE$");
        assert_eq!(state.tiles.size(), Size::new(2, 2));
        assert_eq!(state.origin, Coord::new(1, 1));
        assert_eq!(state.tiles[(0, 0).into()], Directions::E | Directions::S);
        assert_eq!(state.tiles[(1, 0).into()], Directions::W);
        assert_eq!(state.tiles[(0, 1).into()], Directions::N | Directions::E);
        assert_eq!(state.tiles[(1, 1).into()], Directions::W);
    }

    #[test]
    fn cost_map_1() {
        let state = State::from_pattern("^WNE$");
        let map = state.make_cost_map();
        assert_eq!(map.size(), Size::new(2, 2));
        assert_eq!(map[(0, 0).into()], Some(2));
        assert_eq!(map[(1, 0).into()], Some(3));
        assert_eq!(map[(0, 1).into()], Some(1));
        assert_eq!(map[(1, 1).into()], Some(0));
        assert_eq!(largest_cost(&map), Some(3));
    }

    #[test]
    fn from_pattern_2() {
        let state = State::from_pattern("^ENWWW(NEEE|SSE(EE|N))$");
        assert_eq!(state.tiles.size(), Size::new(4, 4));
        assert_eq!(state.origin, Coord::new(2, 2));
        assert_eq!(state.tiles[(2, 2).into()], Directions::E);
        assert_eq!(state.tiles[(3, 2).into()], Directions::W | Directions::N);
        assert_eq!(
            state.tiles[(0, 1).into()],
            Directions::N | Directions::E | Directions::S
        );
        assert_eq!(state.tiles[(3, 3).into()], Directions::W);
    }

    #[test]
    fn cost_map_2() {
        let state = State::from_pattern("^ENWWW(NEEE|SSE(EE|N))$");
        let map = state.make_cost_map();
        assert_eq!(map.size(), Size::new(4, 4));
        assert_eq!(map[(2, 2).into()], Some(0));
        assert_eq!(map[(0, 0).into()], Some(6));
        assert_eq!(map[(0, 1).into()], Some(5));
        assert_eq!(map[(3, 3).into()], Some(10));
        assert_eq!(largest_cost(&map), Some(10));
    }

    #[test]
    fn cost_map_3() {
        let state = State::from_pattern("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$");
        let map = state.make_cost_map();
        assert_eq!(largest_cost(&map), Some(18));
    }

    #[test]
    fn cost_map_4() {
        let state = State::from_pattern("^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$");
        let map = state.make_cost_map();
        assert_eq!(largest_cost(&map), Some(23));
    }

    #[test]
    fn cost_map_5() {
        let state = State::from_pattern(
            "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$",
        );
        let map = state.make_cost_map();
        assert_eq!(largest_cost(&map), Some(31));
    }
}
