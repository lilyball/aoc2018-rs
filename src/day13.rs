use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Coord {
    x: usize,
    y: usize,
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{{{},{}}}", self.x, self.y)
    }
}

#[derive(Clone)]
struct Track {
    cells: Vec<Vec<Cell>>,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum TrackPiece {
    Horizontal,
    Vertical,
    Slash,
    Backslash,
    Intersection,
    Empty,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Cart {
    direction: Direction,
    next_turn: Turn,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Turn {
    Left,
    Straight,
    Right,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Cell {
    track: TrackPiece,
    cart: Option<Cart>,
}

impl Track {
    fn from_str(input: &str) -> Track {
        let cells = input
            .lines()
            .map(|line| line.chars().map(Cell::from_char).collect())
            .collect();
        Track { cells }
    }

    fn at(&self, coord: Coord) -> Cell {
        self.cells[coord.y][coord.x]
    }

    fn mut_at(&mut self, coord: Coord) -> &mut Cell {
        &mut self.cells[coord.y][coord.x]
    }

    /// Moves all carts one tick and returns the coordinates of all collisions.
    /// Each collision immediately removes both carts.
    fn tick(&mut self) -> Vec<Coord> {
        let mut moved = HashSet::new();
        let mut collisions = Vec::new();
        for y in 0..self.cells.len() {
            for x in 0..self.cells[y].len() {
                if moved.contains(&Coord { x, y }) {
                    continue;
                }
                if let Some(cart) = self.at(Coord { x, y }).cart {
                    self.mut_at(Coord { x, y }).cart = None;
                    let coord = cart.step(Coord { x, y });
                    let cart = cart.follow_track(self.at(coord).track);
                    if let Some(_) = std::mem::replace(&mut self.mut_at(coord).cart, Some(cart)) {
                        collisions.push(coord);
                        self.mut_at(coord).cart = None;
                    } else {
                        moved.insert(coord);
                    }
                }
            }
        }
        collisions
    }

    fn count_carts(&self) -> usize {
        self.cells
            .iter()
            .flatten()
            .filter(|cell| cell.cart.is_some())
            .count()
    }

    fn cell_iter(&self) -> impl Iterator<Item = (Coord, &Cell)> {
        self.cells.iter().enumerate().flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .map(move |(x, cell)| (Coord { x, y }, cell))
        })
    }

    #[allow(unused)]
    fn show(&self) -> String {
        self.cells
            .iter()
            .flat_map(|row| {
                row.iter()
                    .map(|cell| match (cell.track, cell.cart.map(|c| c.direction)) {
                        (_, Some(Direction::Right)) => '>',
                        (_, Some(Direction::Up)) => '^',
                        (_, Some(Direction::Left)) => '<',
                        (_, Some(Direction::Down)) => 'v',
                        (TrackPiece::Horizontal, None) => '-',
                        (TrackPiece::Vertical, None) => '|',
                        (TrackPiece::Slash, None) => '/',
                        (TrackPiece::Backslash, None) => '\\',
                        (TrackPiece::Intersection, None) => '+',
                        (TrackPiece::Empty, None) => ' ',
                    })
                    .chain(Some('\n'))
            })
            .collect()
    }
}

impl Cart {
    fn step(&self, from: Coord) -> Coord {
        match self.direction {
            Direction::Up => Coord {
                y: from.y - 1,
                ..from
            },
            Direction::Down => Coord {
                y: from.y + 1,
                ..from
            },
            Direction::Left => Coord {
                x: from.x - 1,
                ..from
            },
            Direction::Right => Coord {
                x: from.x + 1,
                ..from
            },
        }
    }

    fn follow_track(&self, track: TrackPiece) -> Cart {
        match (track, self.direction) {
            (TrackPiece::Horizontal, Direction::Left)
            | (TrackPiece::Horizontal, Direction::Right) => *self,
            (TrackPiece::Horizontal, _) => panic!("{:?} can't be on {:?}", *self, track),
            (TrackPiece::Vertical, Direction::Up) | (TrackPiece::Vertical, Direction::Down) => {
                *self
            }
            (TrackPiece::Vertical, _) => panic!("{:?} can't be on {:?}", *self, track),
            (TrackPiece::Slash, Direction::Up) => self.with_direction(Direction::Right),
            (TrackPiece::Slash, Direction::Right) => self.with_direction(Direction::Up),
            (TrackPiece::Slash, Direction::Left) => self.with_direction(Direction::Down),
            (TrackPiece::Slash, Direction::Down) => self.with_direction(Direction::Left),
            (TrackPiece::Backslash, Direction::Right) => self.with_direction(Direction::Down),
            (TrackPiece::Backslash, Direction::Down) => self.with_direction(Direction::Right),
            (TrackPiece::Backslash, Direction::Left) => self.with_direction(Direction::Up),
            (TrackPiece::Backslash, Direction::Up) => self.with_direction(Direction::Left),
            (TrackPiece::Intersection, _) => Cart {
                direction: self.next_turn.apply(self.direction),
                next_turn: self.next_turn.next(),
            },
            (TrackPiece::Empty, _) => panic!("{:?} can't be on {:?}", *self, track),
        }
    }

    fn with_direction(&self, direction: Direction) -> Cart {
        Cart { direction, ..*self }
    }
}

impl Turn {
    fn apply(&self, direction: Direction) -> Direction {
        match (*self, direction) {
            (Turn::Left, Direction::Up) => Direction::Left,
            (Turn::Left, Direction::Left) => Direction::Down,
            (Turn::Left, Direction::Down) => Direction::Right,
            (Turn::Left, Direction::Right) => Direction::Up,
            (Turn::Straight, _) => direction,
            (Turn::Right, Direction::Up) => Direction::Right,
            (Turn::Right, Direction::Right) => Direction::Down,
            (Turn::Right, Direction::Down) => Direction::Left,
            (Turn::Right, Direction::Left) => Direction::Up,
        }
    }

    fn next(&self) -> Turn {
        match *self {
            Turn::Left => Turn::Straight,
            Turn::Straight => Turn::Right,
            Turn::Right => Turn::Left,
        }
    }
}

impl Cell {
    fn from_char(c: char) -> Cell {
        let (track, cart) = match c {
            ' ' => (TrackPiece::Empty, None),
            '-' => (TrackPiece::Horizontal, None),
            '|' => (TrackPiece::Vertical, None),
            '/' => (TrackPiece::Slash, None),
            '\\' => (TrackPiece::Backslash, None),
            '+' => (TrackPiece::Intersection, None),
            '>' => (
                TrackPiece::Horizontal,
                Some(Cart {
                    direction: Direction::Right,
                    next_turn: Turn::Left,
                }),
            ),
            '<' => (
                TrackPiece::Horizontal,
                Some(Cart {
                    direction: Direction::Left,
                    next_turn: Turn::Left,
                }),
            ),
            '^' => (
                TrackPiece::Vertical,
                Some(Cart {
                    direction: Direction::Up,
                    next_turn: Turn::Left,
                }),
            ),
            'v' => (
                TrackPiece::Vertical,
                Some(Cart {
                    direction: Direction::Down,
                    next_turn: Turn::Left,
                }),
            ),
            _ => panic!("unknown track piece {}", c),
        };
        Cell { track, cart }
    }
}

#[aoc_generator(day13)]
fn input_generator(input: &str) -> Track {
    Track::from_str(input)
}

#[aoc(day13, part1)]
fn part1(input: &Track) -> Coord {
    let mut track = input.clone();
    loop {
        match track.tick().into_iter().next() {
            Some(coord) => break coord,
            None => {}
        }
    }
}

#[aoc(day13, part2)]
fn part2(input: &Track) -> Coord {
    let mut track = input.clone();
    loop {
        match track.tick().into_iter().next() {
            Some(_) if track.count_carts() <= 1 => break,
            _ => {}
        }
    }
    let coord = track
        .cell_iter()
        .filter(|(_, cell)| cell.cart.is_some())
        .map(|(coord, _)| coord)
        .next()
        .expect("no carts left");
    coord
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample_collision() {
        let mut track = Track::from_str(include_str!("../test_data/day13.txt"));
        let coord = loop {
            match track.tick().into_iter().next() {
                Some(coord) => break coord,
                None => {}
            }
        };
        assert_eq!(coord, Coord { x: 7, y: 3 });
    }

    #[test]
    fn test_run_until_one() {
        let mut track = Track::from_str(include_str!("../test_data/day13_2.txt"));
        loop {
            match track.tick().into_iter().next() {
                Some(_) if track.count_carts() <= 1 => break,
                _ => {}
            }
        }
        let coord = track
            .cell_iter()
            .filter(|(_, cell)| cell.cart.is_some())
            .map(|(coord, _)| coord)
            .next()
            .expect("no carts left");
        assert_eq!(coord, Coord { x: 6, y: 4 });
    }
}
