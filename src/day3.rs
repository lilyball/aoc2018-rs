#![allow(dead_code)]

use aoc_runner_derive::{aoc, aoc_generator};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashSet;
use std::default::Default;
use std::iter::repeat;
use std::str::FromStr;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
struct Size {
    width: i32,
    height: i32,
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq)]
struct Rect {
    origin: Point,
    size: Size,
}

impl Rect {
    fn width(&self) -> i32 {
        self.size.width
    }

    fn height(&self) -> i32 {
        self.size.height
    }

    fn min_x(&self) -> i32 {
        self.origin.x
    }

    fn max_x(&self) -> i32 {
        self.min_x() + self.width()
    }

    fn min_y(&self) -> i32 {
        self.origin.y
    }

    fn max_y(&self) -> i32 {
        self.min_y() + self.height()
    }

    fn max_x_y(&self) -> Point {
        Point {
            x: self.max_x(),
            y: self.max_y(),
        }
    }

    fn union(&self, other: Rect) -> Rect {
        let origin = Point {
            x: self.min_x().min(other.min_x()),
            y: self.min_y().min(other.min_y()),
        };
        let bottom_right = Point {
            x: self.max_x().max(other.max_x()),
            y: self.max_y().max(other.max_y()),
        };
        Rect {
            origin,
            size: Size {
                width: bottom_right.x - origin.x,
                height: bottom_right.y - origin.y,
            },
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Claim {
    id: i32,
    rect: Rect,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum ClaimParseError {
    InvalidFormat,
    Overflow,
}

impl FromStr for Claim {
    type Err = ClaimParseError;

    // #123 @ 3,2: 5x4
    fn from_str(s: &str) -> Result<Claim, ClaimParseError> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"^#(\d+)\s*@\s*(\d+),(\d+):\s*(\d+)x(\d+)$").unwrap();
        }
        let caps = RE.captures(s).ok_or(ClaimParseError::InvalidFormat)?;
        let get_int_at = |idx| {
            caps.get(idx)
                .unwrap()
                .as_str()
                .parse()
                .or(Err(ClaimParseError::Overflow))
        };
        let id = get_int_at(1)?;
        let left = get_int_at(2)?;
        let top = get_int_at(3)?;
        let width = get_int_at(4)?;
        let height = get_int_at(5)?;
        let origin = Point { x: left, y: top };
        let size = Size { width, height };
        Ok(Claim {
            id,
            rect: Rect { origin, size },
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_claim() {
        assert_eq!(
            "#123 @ 3,2: 5x4".parse(),
            Ok(Claim {
                id: 123,
                rect: Rect {
                    origin: Point { x: 3, y: 2 },
                    size: Size {
                        width: 5,
                        height: 4
                    }
                }
            })
        );
    }
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<Claim> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day3, part1)]
fn part1(input: &[Claim]) -> usize {
    let fabric_size = min_fabric_size(input);
    let mut fabric = repeat(repeat(0).take(fabric_size.width as usize).collect())
        .take(fabric_size.height as usize)
        .collect::<Vec<Vec<_>>>();
    for claim in input {
        for x in claim.rect.min_x()..claim.rect.max_x() {
            for y in claim.rect.min_y()..claim.rect.max_y() {
                fabric[y as usize][x as usize] += 1;
            }
        }
    }
    fabric
        .into_iter()
        .map(|row| row.into_iter().filter(|&x| x > 1).count())
        .sum()
}

// The minimum fabric size needed to cover all claims.
fn min_fabric_size(claims: &[Claim]) -> Size {
    claims
        .iter()
        .fold(<Rect as Default>::default(), |rect, claim| {
            rect.union(claim.rect)
        })
        .size
}

#[aoc(day3, part2)]
fn part2(input: &[Claim]) -> i32 {
    assert!(
        input.iter().find(|claim| claim.id == 0).is_none(),
        "there's a claim with id #0"
    );
    let fabric_size = min_fabric_size(input);
    // Each square of the fabric records the id of the first claim that covers it.
    let mut fabric = repeat(repeat(0i32).take(fabric_size.width as usize).collect())
        .take(fabric_size.height as usize)
        .collect::<Vec<Vec<_>>>();
    // Records each claim we know that overlaps at least one other claim.
    let mut overlapping = HashSet::new();
    // Mark the fabric up
    // If we detect an overlap, insert both overlapping claims in the set.
    for claim in input {
        for x in claim.rect.min_x()..claim.rect.max_x() {
            for y in claim.rect.min_y()..claim.rect.max_y() {
                let prev_claim = fabric[y as usize][x as usize];
                if prev_claim != 0 {
                    overlapping.insert(prev_claim);
                    overlapping.insert(claim.id);
                } else {
                    fabric[y as usize][x as usize] = claim.id;
                }
            }
        }
    }
    // Iterate our claims looking for one we never marked as overlapping
    input
        .iter()
        .map(|x| x.id)
        .find(|id| !overlapping.contains(id))
        .unwrap()
}
