use crate::scanner::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::iter::repeat;

type RegValue = i32;
type Instruction = [i8; 4];
type Registers = [RegValue; 4];

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Sample {
    before: Registers,
    instruction: Instruction,
    after: Registers,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Opcode {
    addr,
    addi,
    mulr,
    muli,
    banr,
    bani,
    borr,
    bori,
    setr,
    seti,
    gtir,
    gtri,
    gtrr,
    eqir,
    eqri,
    eqrr,
}

impl Opcode {
    const ALL: [Opcode; 16] = [
        Opcode::addr,
        Opcode::addi,
        Opcode::mulr,
        Opcode::muli,
        Opcode::banr,
        Opcode::bani,
        Opcode::borr,
        Opcode::bori,
        Opcode::setr,
        Opcode::seti,
        Opcode::gtir,
        Opcode::gtri,
        Opcode::gtrr,
        Opcode::eqir,
        Opcode::eqri,
        Opcode::eqrr,
    ];

    fn execute(self, mut registers: Registers, input1: i8, input2: i8, output: i8) -> Registers {
        let out_value = match self {
            Opcode::addr => registers[input1 as usize] + registers[input2 as usize],
            Opcode::addi => registers[input1 as usize] + input2 as RegValue,
            Opcode::mulr => registers[input1 as usize] * registers[input2 as usize],
            Opcode::muli => registers[input1 as usize] * input2 as RegValue,
            Opcode::banr => registers[input1 as usize] & registers[input2 as usize],
            Opcode::bani => registers[input1 as usize] & input2 as RegValue,
            Opcode::borr => registers[input1 as usize] | registers[input2 as usize],
            Opcode::bori => registers[input1 as usize] | input2 as RegValue,
            Opcode::setr => registers[input1 as usize],
            Opcode::seti => input1 as RegValue,
            Opcode::gtir => (input1 as RegValue > registers[input2 as usize]) as RegValue,
            Opcode::gtri => (registers[input1 as usize] > input2 as RegValue) as RegValue,
            Opcode::gtrr => (registers[input1 as usize] > registers[input2 as usize]) as RegValue,
            Opcode::eqir => (input1 as RegValue == registers[input2 as usize]) as RegValue,
            Opcode::eqri => (registers[input1 as usize] == input2 as RegValue) as RegValue,
            Opcode::eqrr => (registers[input1 as usize] == registers[input2 as usize]) as RegValue,
        };
        registers[output as usize] = out_value;
        registers
    }
}

impl Sample {
    /// Tests if the sample behaves like a given opcode.
    fn test_opcode(&self, opcode: Opcode) -> bool {
        opcode.execute(
            self.before,
            self.instruction[1],
            self.instruction[2],
            self.instruction[3],
        ) == self.after
    }
}

#[aoc_generator(day16)]
fn input_generator(input: &str) -> (Vec<Sample>, Vec<Instruction>) {
    let mut scanner = Scanner::new(input);
    scanner.set_chars_to_skip(Some(&|c| c == ' ' || c == ','));
    let mut samples = Vec::new();
    while scanner.scan_str("Before:") {
        assert!(scanner.scan_str("["));
        let mut before = Registers::default();
        for slot in &mut before {
            *slot = scanner.scan().unwrap();
        }
        assert!(scanner.scan_str("]\n"));
        let mut instruction = Instruction::default();
        for slot in &mut instruction {
            *slot = scanner.scan().unwrap();
        }
        assert!(scanner.scan_str("\nAfter:"));
        assert!(scanner.scan_str("["));
        let mut after = Registers::default();
        for slot in &mut after {
            *slot = scanner.scan().unwrap();
        }
        assert!(scanner.scan_str("]\n\n"));
        samples.push(Sample {
            before,
            instruction,
            after,
        });
    }
    assert!(scanner.scan_str("\n"));
    let mut instructions = Vec::new();
    while scanner.scan_str("\n") {
        let mut instruction = Instruction::default();
        for slot in &mut instruction {
            *slot = scanner.scan().unwrap();
        }
        instructions.push(instruction);
    }
    assert!(scanner.is_at_end());
    (samples, instructions)
}

#[aoc(day16, part1)]
fn part1(input: &(Vec<Sample>, Vec<Instruction>)) -> usize {
    input
        .0
        .iter()
        .filter(|sample| {
            Opcode::ALL
                .iter()
                .filter(|&&opcode| sample.test_opcode(opcode))
                .nth(2)
                .is_some()
        })
        .count()
}

#[aoc(day16, part2)]
fn part2(input: &(Vec<Sample>, Vec<Instruction>)) -> i32 {
    let &(ref samples, ref instructions) = input;
    let opcodes = {
        // Map opcodes to instructions
        let mut opcodes = repeat(Vec::from(&Opcode::ALL[..]))
            .take(Opcode::ALL.len())
            .collect::<Vec<_>>();
        for sample in samples {
            let candidates = &mut opcodes[sample.instruction[0] as usize];
            if candidates.len() > 1 {
                candidates.retain(|&opcode| sample.test_opcode(opcode));
                assert!(!candidates.is_empty());
            }
        }
        // Now that we've processed all samples, reduce the opcode set repeatedly until we can't assign
        // individual opcodes to instructions anymore.
        let mut opcode_map = opcodes
            .iter()
            .map(|candidates| {
                if candidates.len() == 1 {
                    Some(candidates[0])
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        while opcode_map.iter().any(|op| op.is_none()) {
            // Strip known opcodes from the candidates lists
            for slot in &opcode_map {
                let opcode = match slot {
                    &Some(opcode) => opcode,
                    None => continue,
                };
                for candidates in opcodes.iter_mut() {
                    candidates.retain(|&op| op != opcode);
                }
            }
            // Update opcode map
            let mut modified = false;
            for (i, candidates) in opcodes.iter().enumerate() {
                if candidates.len() == 1 {
                    let opcode = candidates[0];
                    match opcode_map[i] {
                        Some(op) => assert!(op == opcode),
                        None => {
                            opcode_map[i] = Some(opcode);
                            modified = true;
                        }
                    }
                }
            }
            assert!(modified, "Opcode map reduction failed");
        }
        opcode_map
            .into_iter()
            .map(|op| op.unwrap())
            .collect::<Vec<_>>()
    };
    // Run the sample program
    let mut registers = Registers::default();
    for instruction in instructions {
        registers = opcodes[instruction[0] as usize].execute(
            registers,
            instruction[1],
            instruction[2],
            instruction[3],
        );
    }
    registers[0]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_input_generator() {
        let (samples, instructions) = input_generator(
            r"Before: [3, 3, 2, 3]
3 1 2 2
After:  [3, 3, 2, 3]

Before: [1, 3, 0, 1]
12 0 2 3
After:  [1, 3, 0, 0]

Before: [0, 3, 2, 0]
14 2 3 0
After:  [1, 3, 2, 0]



1 2 3 4
4 3 2 1",
        );
        assert_eq!(
            &samples,
            &[
                Sample {
                    before: [3, 3, 2, 3],
                    instruction: [3, 1, 2, 2],
                    after: [3, 3, 2, 3]
                },
                Sample {
                    before: [1, 3, 0, 1],
                    instruction: [12, 0, 2, 3],
                    after: [1, 3, 0, 0]
                },
                Sample {
                    before: [0, 3, 2, 0],
                    instruction: [14, 2, 3, 0],
                    after: [1, 3, 2, 0]
                }
            ]
        );
        assert_eq!(instructions, &[[1, 2, 3, 4], [4, 3, 2, 1]]);
    }
}
