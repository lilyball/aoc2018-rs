use crate::grid::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::cmp::Ordering;
use std::collections::HashSet;
use std::iter::repeat_with;

#[derive(Clone)]
struct Map {
    tiles: Grid<Tile>,
    creatures: Vec<Creature>,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Tile {
    Empty,
    Wall,
    Creature(usize),
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Creature {
    race: Race,
    hp: isize,
    power: isize,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Race {
    Goblin,
    Elf,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Cost {
    Cost(usize),
    Wall,
    Creature(Race),
}

impl Creature {
    fn new(race: Race, attack_power: isize) -> Creature {
        Creature {
            race,
            hp: 200,
            power: attack_power,
        }
    }
}

impl Race {
    fn as_char(&self) -> char {
        match *self {
            Race::Goblin => 'G',
            Race::Elf => 'E',
        }
    }
}

impl Cost {
    fn as_char(&self) -> char {
        match *self {
            Cost::Cost(cost) => std::char::from_digit(cost as u32, 36).unwrap_or('?'),
            Cost::Wall => '#',
            Cost::Creature(race) => race.as_char(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct CreatureSummary {
    remaining_goblins: usize,
    remaining_elves: usize,
    remaining_hp: usize,
}

impl Map {
    fn from_str(input: &str, goblin_attack_power: isize, elf_attack_power: isize) -> Map {
        let width = input.lines().map(|line| line.len()).max().unwrap();
        let height = input.lines().count();
        let mut tiles = Grid::new_clone(Size::new(width as i32, height as i32), &Tile::Empty);
        let mut creatures = Vec::new();
        for (y, line) in input.lines().enumerate() {
            for (x, byte) in line.bytes().enumerate() {
                tiles[Coord::new(x as i32, y as i32)] = match byte {
                    b'.' => Tile::Empty,
                    b'#' => Tile::Wall,
                    b'G' => {
                        creatures.push(Creature::new(Race::Goblin, goblin_attack_power));
                        Tile::Creature(creatures.len() - 1)
                    }
                    b'E' => {
                        creatures.push(Creature::new(Race::Elf, elf_attack_power));
                        Tile::Creature(creatures.len() - 1)
                    }
                    _ => panic!("Unknown tile type {}", byte as char),
                };
            }
        }
        Map { tiles, creatures }
    }

    /// Runs a full turn.
    /// Returns `true` if the turn completed or `false` if combat ended early.
    fn turn(&mut self) -> bool {
        let creature_order = self
            .tiles
            .iter()
            .filter_map(|(coord, tile)| match tile {
                &Tile::Creature(id) => Some((coord, id)),
                _ => None,
            })
            .collect::<Vec<_>>();
        if creature_order.is_empty() {
            return false;
        }
        let mut cost_map = Grid::new_default(self.tiles.size());
        let mut took_action = false;
        for (mut coord, id) in creature_order {
            let creature = self.creatures[id];
            if creature.hp <= 0 {
                continue;
            } // it's dead, jim
            let enemy_race = match creature.race {
                Race::Goblin => Race::Elf,
                Race::Elf => Race::Goblin,
            };
            let targets = self
                .tiles
                .iter()
                .filter_map(|(coord, tile)| match tile {
                    &Tile::Creature(id) if self.creatures[id].race == enemy_race => {
                        Some((coord, id))
                    }
                    _ => None,
                })
                .collect::<Vec<_>>();
            if targets.is_empty() {
                // No more targets; combat ends
                return false;
            }
            // # Movement
            const OFFSETS: [Coord; 4] = [
                Coord::new(0, -1),
                Coord::new(-1, 0),
                Coord::new(1, 0),
                Coord::new(0, 1),
            ];
            let open_spots = targets
                .iter()
                .flat_map(|&(coord, _)| OFFSETS.iter().map(move |offset| coord + offset))
                .filter(|&coord| {
                    self.tiles.get(coord).map_or(false, |&tile| match tile {
                        Tile::Empty => true,
                        Tile::Creature(id_) if id == id_ => true,
                        _ => false,
                    })
                })
                .collect::<HashSet<_>>();
            if open_spots.is_empty() {
                // No targets have open spots next to them; turn ends
                continue;
            }
            if !open_spots.contains(&coord) {
                self.fill_cost_map(&mut cost_map, coord);
                let best_spot = match open_spots
                    .iter()
                    .filter_map(|&coord| match cost_map[coord] {
                        Some(Cost::Cost(cost)) => Some((coord, cost)),
                        _ => None,
                    })
                    .min_by(|&(a_coord, a_cost), &(b_coord, b_cost)| {
                        a_cost
                            .cmp(&b_cost)
                            .then_with(|| cmp_reading_order(a_coord, b_coord))
                    }) {
                    Some((spot, _)) => spot,
                    None => {
                        // No open spots are reachable; turn ends
                        continue;
                    }
                };
                let first_step = self.first_step_from_cost_map(&cost_map, best_spot);
                assert_eq!(self.tiles[first_step], Tile::Empty);
                self.tiles.swap(coord, first_step);
                coord = first_step;
                took_action = true;
            }
            // # Attack
            let close_targets = targets
                .iter()
                .filter(|&&(target_coord, _)| distance(coord, target_coord) <= 1);
            match close_targets.min_by(|&&(a_coord, a_id), &&(b_coord, b_id)| {
                self.creatures[a_id]
                    .hp
                    .cmp(&self.creatures[b_id].hp)
                    .then_with(|| cmp_reading_order(a_coord, b_coord))
            }) {
                Some(&(target_coord, target_id)) => {
                    self.creatures[target_id].hp -= self.creatures[id].power;
                    if self.creatures[target_id].hp <= 0 {
                        // it's dead
                        assert_eq!(self.tiles[target_coord], Tile::Creature(target_id));
                        self.tiles[target_coord] = Tile::Empty;
                    }
                    took_action = true;
                }
                None => {}
            }
        }
        assert!(took_action, "turn ended with no actions taken");
        true
    }

    /// Runs turns until combat ends and returns the number of full turns.
    fn run_to_end(&mut self) -> usize {
        repeat_with(move || self.turn()).take_while(|&x| x).count()
    }

    fn creature_summary(&self) -> CreatureSummary {
        let living_creatures = self.creatures.iter().filter(|&c| c.hp > 0);
        CreatureSummary {
            remaining_goblins: living_creatures
                .clone()
                .filter(|&c| c.race == Race::Goblin)
                .count(),
            remaining_elves: living_creatures
                .clone()
                .filter(|&c| c.race == Race::Elf)
                .count(),
            remaining_hp: living_creatures.map(|&c| c.hp).sum::<isize>() as usize,
        }
    }

    /// Fills a given cost map with the distance of each cell from a given coordinate.
    /// Cells not reachable will be left at `None`.
    /// The cost map must have the same size as `self.tiles`.
    fn fill_cost_map(&self, cost_map: &mut Grid<Option<Cost>>, from: Coord) {
        for (coord, cost) in cost_map.iter_mut() {
            *cost = match self.tiles[coord] {
                Tile::Wall => Some(Cost::Wall),
                Tile::Creature(id) => Some(Cost::Creature(self.creatures[id].race)),
                Tile::Empty => None,
            };
        }
        cost_map[from] = Some(Cost::Cost(0));
        const OFFSETS: [Coord; 4] = [
            Coord::new(0, -1),
            Coord::new(-1, 0),
            Coord::new(1, 0),
            Coord::new(0, 1),
        ];
        for next_cost in 1.. {
            let mut made_change = false;
            for coord in cost_map.iter_coords() {
                if cost_map[coord] == Some(Cost::Cost(next_cost - 1)) {
                    for new_coord in OFFSETS.iter().map(|offset| coord + offset) {
                        if cost_map[new_coord].is_none() {
                            cost_map[new_coord] = Some(Cost::Cost(next_cost));
                            made_change = true;
                        }
                    }
                }
            }
            if !made_change {
                break;
            }
        }
    }

    /// Returns the first step to take on the cost map to get to `to`.
    /// Panics if the cost map does not contain a path that leads to `to`.
    fn first_step_from_cost_map(&self, cost_map: &Grid<Option<Cost>>, mut to: Coord) -> Coord {
        let mut cost = match cost_map[to] {
            Some(Cost::Cost(cost)) => cost,
            _ => panic!("cost map does not include cost for destination"),
        };
        // Walk the gradient backwards, always prioritizing stepping in reading order, until we hit
        // a position of cost `1`.
        const OFFSETS: [Coord; 4] = [
            Coord::new(0, -1),
            Coord::new(-1, 0),
            Coord::new(1, 0),
            Coord::new(0, 1),
        ];
        while cost > 1 {
            #[cfg(debug_assertions)]
            let old_cost = cost;
            for coord in OFFSETS.iter().map(move |offset| to + offset) {
                match cost_map[coord] {
                    Some(Cost::Cost(new_cost)) if new_cost < cost => {
                        debug_assert!(new_cost == cost - 1, "cost map discontinuity");
                        cost = new_cost;
                        to = coord;
                        break;
                    }
                    _ => {}
                }
            }
            #[cfg(debug_assertions)]
            debug_assert!(old_cost != cost, "invalid cost map");
        }
        to
    }

    #[allow(unused)]
    fn show(&self) -> String {
        use std::fmt::Write;
        let (mut s, suffix) = self.tiles.iter().fold(
            (
                String::with_capacity(
                    (self.tiles.width() as usize + 1) * self.tiles.height() as usize,
                ),
                String::new(),
            ),
            |(mut s, mut suffix), (coord, &tile)| {
                if coord.x == 0 && coord.y > 0 {
                    s.push_str(&suffix);
                    suffix.clear();
                    s.push('\n');
                }
                s.push(match tile {
                    Tile::Empty => '.',
                    Tile::Wall => '#',
                    Tile::Creature(id) => {
                        suffix.push_str(if suffix.is_empty() { "   " } else { ", " });
                        let creature = &self.creatures[id];
                        write!(&mut suffix, "{}({})", creature.race.as_char(), creature.hp);
                        self.creatures[id].race.as_char()
                    }
                });
                (s, suffix)
            },
        );
        s.push_str(&suffix);
        s.push('\n');
        s
    }
}

#[allow(unused)]
fn show_costs(costs: &Grid<Option<Cost>>) -> String {
    costs.iter().fold(
        String::with_capacity((costs.width() as usize + 1) * costs.height() as usize),
        |mut s, (coord, &cost)| {
            if coord.x == 0 && coord.y > 0 {
                s.push('\n');
            }
            s.push(match cost {
                Some(cost) => cost.as_char(),
                None => '.',
            });
            s
        },
    )
}

/// Manhattan distance
fn distance(from: Coord, to: Coord) -> usize {
    (from.x - to.x).abs() as usize + (from.y - to.y).abs() as usize
}

fn cmp_reading_order(a: Coord, b: Coord) -> Ordering {
    a.y.cmp(&b.y).then(a.x.cmp(&b.x))
}

#[aoc_generator(day15)]
fn input_generator(input: &str) -> Map {
    Map::from_str(input, 3, 3)
}

#[aoc(day15, part1)]
fn part1(input: &Map) -> usize {
    let mut map = input.clone();
    let full_turns = map.run_to_end();
    let remaining_hp = map.creature_summary().remaining_hp;
    full_turns * remaining_hp
}

#[aoc(day15, part2)]
fn part2(input: &Map) -> usize {
    let mut elf_power = 3;
    let mut map = input.clone();
    let initial_elf_count = map.creature_summary().remaining_elves;
    loop {
        let full_turns = map.run_to_end();
        let CreatureSummary {
            remaining_elves,
            remaining_hp,
            ..
        } = map.creature_summary();
        if remaining_elves == initial_elf_count {
            return full_turns * remaining_hp;
        }
        elf_power += 1;
        map = input.clone();
        map.creatures
            .iter_mut()
            .filter(|c| c.race == Race::Elf)
            .for_each(|c| c.power = elf_power);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn combat_1() {
        let mut map = Map::from_str(include_str!("../test_data/day15_1.txt"), 3, 3);
        assert_eq!(map.run_to_end(), 47);
        assert_eq!(map.creature_summary().remaining_hp, 590);
    }

    #[test]
    fn combat_2() {
        let mut map = Map::from_str(include_str!("../test_data/day15_2.txt"), 3, 3);
        assert_eq!(map.run_to_end(), 37);
        assert_eq!(map.creature_summary().remaining_hp, 982);
    }

    #[test]
    fn combat_3() {
        let mut map = Map::from_str(include_str!("../test_data/day15_3.txt"), 3, 3);
        assert_eq!(map.run_to_end(), 46);
        assert_eq!(map.creature_summary().remaining_hp, 859);
    }

    #[test]
    fn combat_4() {
        let mut map = Map::from_str(include_str!("../test_data/day15_4.txt"), 3, 3);
        assert_eq!(map.run_to_end(), 54);
        assert_eq!(map.creature_summary().remaining_hp, 536);
    }

    #[test]
    fn combat_5() {
        let mut map = Map::from_str(include_str!("../test_data/day15_5.txt"), 3, 3);
        assert_eq!(map.run_to_end(), 20);
        assert_eq!(map.creature_summary().remaining_hp, 937);
    }

    #[test]
    fn combat_1_elf_power_15() {
        let mut map = Map::from_str(include_str!("../test_data/day15_1.txt"), 3, 15);
        assert_eq!(map.run_to_end(), 29);
        assert_eq!(
            map.creature_summary(),
            CreatureSummary {
                remaining_goblins: 0,
                remaining_elves: 2,
                remaining_hp: 172
            }
        );
    }

    #[test]
    fn combat_4_elf_power_12() {
        let mut map = Map::from_str(include_str!("../test_data/day15_4.txt"), 3, 12);
        assert_eq!(map.run_to_end(), 39);
        assert_eq!(
            map.creature_summary(),
            CreatureSummary {
                remaining_goblins: 0,
                remaining_elves: 2,
                remaining_hp: 166
            }
        );
    }

    #[test]
    fn combat_5_elf_power_34() {
        let mut map = Map::from_str(include_str!("../test_data/day15_5.txt"), 3, 34);
        assert_eq!(map.run_to_end(), 30);
        assert_eq!(
            map.creature_summary(),
            CreatureSummary {
                remaining_goblins: 0,
                remaining_elves: 1,
                remaining_hp: 38
            }
        );
    }

    #[test]
    fn first_step_from_cost_map() {
        let map = Map::from_str(include_str!("../test_data/day15_1.txt"), 3, 3);
        let mut cost_map = Grid::new_default(map.tiles.size());
        map.fill_cost_map(&mut cost_map, Coord::new(3, 4));
        assert_eq!(
            map.first_step_from_cost_map(&cost_map, Coord::new(5, 5)),
            Coord::new(3, 5)
        );
        map.fill_cost_map(&mut cost_map, Coord::new(4, 2));
        assert_eq!(
            map.first_step_from_cost_map(&cost_map, Coord::new(3, 1)),
            Coord::new(4, 1)
        );
        assert_eq!(
            map.first_step_from_cost_map(&cost_map, Coord::new(2, 2)),
            Coord::new(3, 2)
        );
    }
}
